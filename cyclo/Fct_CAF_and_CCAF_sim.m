function [CAF, CCAF, time_ax_CAF]=Fct_CAF_and_CCAF_sim(fs, alpha_lag,TxDataBit, window_type, FFTWin_length);
%fs is sampling frequency in samples, eg Ns*NBOC in CDMA
%alpha_lag is the vector of the cyclic frequencies of interest
%TxDataBit is the signal whose CAF we want to compute
%window_type is a window flag (1,2,or 3) stating whether we use any
%windowing or not. Option 1=no windo, option 2=Hanning window; option
%3=hamming window. The case 1 was tested, the others not so much
%FFTWin_length: size  over which we do the correlation; it  is related with
%the accuracy/resolution, but we don't have yet a good understanding about
%what value is optimum
    
    Ts=1/fs;
    Len1=length(TxDataBit);
    time_ax=[0:Len1-1]*Ts;
    Len_caf=min([FFTWin_length  length(TxDataBit)]);
    time_ax_CAF=[-Len_caf:Len_caf]*Ts;
    
    
    switch window_type
        case 1
            Han_window=ones(1,Len1);
        case 2
            Han_window=hanning(Len1).';
        case 3
            Han_window=hamming(Len1).';
        case 4
            Han_window=kaiser(Len1,10);
    end;
    
    Len=2*Len_caf+1;
    CAF=zeros(length(alpha_lag), Len);
    CCAF=zeros(length(alpha_lag), Len);
    for  alph=1:length(alpha_lag),
        %pay attention whether we have 2 or 1 as factors
        expo_term_plus=exp(+j*pi*time_ax*alpha_lag(alph));
        expo_term_minus=exp(-j*pi*time_ax*alpha_lag(alph));
        %cyclic autocorrelation function  is acf_caf
        s1=Han_window.*TxDataBit.*expo_term_minus;
        s2=Han_window.*TxDataBit.*expo_term_plus;
        s2conj=Han_window.*conj(TxDataBit).*expo_term_plus;
        %do xcorr only on min([FFTWin_length  length(s1)]) lags
        CAF(alph,:)=xcorr(s1, s2, min([FFTWin_length  length(s1)]))';
        CCAF(alph,:)=xcorr(s1, s2conj, min([FFTWin_length  length(s1)]))';
    end;
end


