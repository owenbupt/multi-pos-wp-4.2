function [CAF, SCF_short, fax_short, alpha_lag_short]=Fct_FAM_SCF_fin5Feb14(fs, TxDataBit_init,  delta_f, delta_alph,...
    alpha_lag_short_bounds, fax_short_bounds);
%fs is sampling frequency in samples, eg Ns*NBOC in CDMA
%TxDataBit is the signal whose CAF we want to compute
%delta_f is the time step in frequency
%delta_alph is the time step in cyclic frequency (usually should be much
%smaller than delta_f)
%alpha_lag_short_bounds is the vector with 2 elements showing the bounds (min and max) of the cyclic frequencies of interest
%fax_short_bounds is the vector with 2 elements showing the bounds (min and max) of the  frequencies of interest


Nprim=pow2(nextpow2(fs/delta_f));
%some studies Prof. Loomis paper said that L=fix(Nprim/4) is best value; L is the amount of
%non-overlapping
L=fix(0.25*Nprim);
P=pow2(nextpow2(fs/L/delta_alph)); 
N=P*L; 
if (Nprim+1)*(2*N+1)>5e7,
    P*Nprim^2
    (Nprim+1)*(2*N+1)
    error('Decrease fs or resolution, numbers too high')
end;
%SCF=zeros(Nprim+1,2*N+1); 

%size of TxDataBit should be at least (P-1)*L+Nprim => pad with zero
Full_Len=P*L+Nprim;
Len=length(TxDataBit_init);

NsignalPieces=fix(Len/Full_Len);
if Len<Full_Len,
    %extend TxDataBit_init with zeros
    TxDataBit=[TxDataBit_init zeros(1,Full_Len-Len)];
else
    TxDataBit=TxDataBit_init(1:Full_Len*NsignalPieces);
end;
SCF=0;
CAF=0;
for nn=1:max([1 NsignalPieces]), %
    %for each signal piece
    %group signal in pieces of Nprim length
    sig_extract=zeros(Nprim,P);
    for k=0:P-1,
            % alpha_lag(k+1)=k*L; 
             sig_extract(:,k+1)=TxDataBit((nn-1)*Full_Len+k*L+1:(nn-1)*Full_Len+k*L+Nprim);
    end;
    %windowing 
    a=hamming(Nprim); 
    XW=diag(a)*sig_extract; 


    %first FFT 
    XF1=fftshift(fft(XW)); 
    XF1=[XF1(:,P/2+1:P) XF1(:,1:P/2)]; 

    %downconversions 
    E=zeros(Nprim,P); 
    for k=-Nprim/2:Nprim/2-1 
        for k0=0:P-1 
            E(k+Nprim/2+1,k0+1)=exp(-j*2*pi*k*k0*L/Nprim);   
        end 
    end 
    XD=XF1.*E; 
    XD=conj(XD'); 

    %multiplication 
    XM=zeros(P,Nprim^2); 
    for k=1:Nprim 
        for c=1:Nprim 
            XM(:,(k-1)*Nprim+c)=(XD(:,k).*conj(XD(:,c))); 
        end 
    end 

    
    %second FFT 
    XF2=fftshift(fft(XM)); 
    XF2=[XF2(:,Nprim^2/2+1:Nprim^2) XF2(:,1:Nprim^2/2)]; 
    XF2=XF2(P/4:3*P/4,:); 
    M=abs(XF2); 
    alpha_lag=-fs:fs/N:fs; 
    fax=-fs/2:fs/Nprim:fs/2; 
    %Sx=zeros(Nprim+1,2*N+1); 
    Sx=NaN*ones(Nprim+1,2*N+1); 
    for k1=1:P/2+1 
        for k2=1:Nprim^2 
            if rem(k2,Nprim)==0 
                c=Nprim/2-1; 
            else 
                c=rem(k2,Nprim)-Nprim/2-1; 
            end 
            k=ceil(k2/Nprim)-Nprim/2-1; 
            p=k1-P/4-1; 
            alph=(k-c)/Nprim+(p-1)/L/P; 
            f=(k+c)/2/Nprim; 
            if alph<-1 | alph>1 
                k2=k2+1; 
            elseif f<-.5 | f>.5 
                k2=k2+1; 
            else 
                kk=1+Nprim*(f+.5); 
                ll=1+N*(alph+1); 
                Sx(round(kk),round(ll))=M(k1,k2); 
            end 
        end 
    end  
    %SCF(f,alph)
    %SCF=Sx./max(max(Sx));  %updated 4th of February: removing the
    %no normalization 
    SCF=SCF+Sx/max([1 NsignalPieces]);
    CAF=CAF+XM/max([1 NsignalPieces]); 
end;

%keep only a smaller part of SCF

[~, a1]=min(abs(alpha_lag-alpha_lag_short_bounds(1)));
[~, a2]=min(abs(alpha_lag-alpha_lag_short_bounds(2)));

[~,f1]=min(abs(fax-fax_short_bounds(1)));
[~,f2]=min(abs(fax-fax_short_bounds(2)));
SCF_short=SCF(f1:f2, a1:a2);
fax_short=fax(f1:f2);
alpha_lag_short=alpha_lag(a1:a2);


