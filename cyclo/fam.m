function [Sx,ao,fo] = fam(input,fs,df,da)
%SSCA obtains the INPUT SCF using the fam method
%
% INPUT
% data - input signal
% fs   - sampling frequency
% df   - frequency resolution
% da   - alpha resolution
%
% CREDITS:
% E. L. Da Costa, "Detection and Identification of
% Cyclostationary Signals". MS Thesis. 1996.
%
% Pedro Silva
% Tampere University of Technology


    % Definition of Parameters
    Np = pow2(nextpow2(fs/df));     % must be power of 2, window time Tw in discrete domain, Eq (18)
    L  = Np/4;                      % number of overlap samples, it should be <= Np/4
    P  = pow2(nextpow2(fs/da/L)); % Eq (19), we can also get N = P*L
    N  = P*L;                       % observation time T in discrete domain
    ao = (-1:1/N:1).*fs;            % cyclic frequency (alpha)
    fo = ((-1/2):1/Np:(1/2)).*fs;   % spectral frequency (f)
    
    % Input vector processing, the length of input vector will be made to N
    if length(input)<N
        input(N) = 0;      % pad zeros
    else
        input = input(1:N); % truncate input to length N
    end

    % Implement Figure 3 in (1): Computation of the Complex Demodulates
    % Construct input matrix in Figure 3, call it x
    NN = fix((P-1)*L+Np);            % number of elements in x matrix
    xx = input;
    if length(xx)<NN
        xx(NN) = 0;             % pad zeros
    else
        xx = xx(1:NN);          % truncate input to length NN
    end
    x = zeros(Np,P);            % x matrix initialization
    for k = 0:P-1
        x(:,k+1) = xx(k*L+1:k*L+Np);
    end

    % Windowing
    a  = hamming(Np);        % Construct N'-point Hamming window
    XW = diag(a)*x;          % Output of N'-point Hamming window

    % Centered N'-point FFT (zero frequency in the 0 bin)
    XF1 = fftshift(fft(XW));
    XF1 = [XF1(:,P/2+1:P) XF1(:,1:P/2)];

    % Downconversion
    E = zeros(Np,P);
    for k = -Np/2:Np/2-1    % length of l is Np
        for m = 0:P-1          % length of k is P
            E(k+Np/2+1,m+1) = exp(-1i*2*pi*m*L*k/Np);
        end
    end
    XD = XF1.*E;                 % Output of Figure 3
    XD = conj(XD');


    % Multiplication
    XM = zeros(P,Np^2); %note the difference to SSCA
    for k = 1:Np
        for c=1:Np
            XM(:,(k-1)*Np+c) = (XD(:,k).*conj(XD(:,c)));
        end
    end


    % Second FFT
    XF2 = fftshift(fft(XM));
    XF2 = [XF2(:,Np^2/2+1:Np^2) XF2(:,1:Np^2/2)];
    XF2 = XF2(P/4:3*P/4,:);
    M   = XF2;

    % Convert M to Sx
    Sx = NaN(Np+1,2*N+1);
    for k1 = 1:P/2+1
        for k2 = 1:(Np^2)
            if rem(k2,Np) == 0
                r = Np/2-1;
            else
                r = rem(k2,Np)-Np/2-1;
            end
            k     = ceil(k2/Np)-Np/2-1;
            p     = k1-P/4-1;
            alpha = (k-r)/Np+(p-1)/L/P;
            f     = (k+r)/2/Np;
            if alpha <-1 || alpha > 1
                k2 = k2+1;
            elseif f <-0.5 || f > 0.5
                k2 = k2+1;
            else
                kk        = 1+Np*(f+0.5);
                ll        = 1+N*(alpha+1);
                Sx(round(kk),round(ll)) = M(k1,k2);
            end
        end
    end
end


% k2 = 1:Np;
% a = (k1-1)/N + (k2-1)/Np - 1;
% f = ((k2-1)/Np - (k1-1)/N) / 2;
% k = 1 + Np*(f+1/2);
% r = 1 + N*(a+1);
% for k2 = 1:Np
%     Sx(round(k(k2)),round(r(k2))) = M(k1,k2);
% end


