function [Sx,ao,fo] = ssca(input,fs,df,da)
%SSCA obtains the INPUT SCF using the SSCA method
%
% INPUT
%   data - input signal
%   fs   - sampling frequency
%   df   - frequency resolution
%   da   - alpha resolution
%
% OUTPUT
%   SCF  - SCF(f,alpha)
%   ao   - Alpha axis
%   fo   - Frequency axis
%
% CREDITS:
%   This function is written according to (1) Figure 3 and Figure 5 of the
%   reference: E. L. Da Costa, "Detection and Identification of
%   Cyclostationary Signals". MS Thesis. 1996. (2) Section 3.2 of the
%   reference: Eric April, "On the Implementation of the Strip Spectral
%   Correlation Algorithm for Cyclic Spectrum Estimation". February, 1994.
% Copyright 2012 The MathWorks, Inc.
%
% Pedro Silva
% Tampere University of Technology


    % Definition of Parameters
    Np = pow2(nextpow2(fs/df));     % must be power of 2, window time Tw in discrete domain, Eq (18)
    if Np < 4
        Np = 4;
        disp('Forced Np to 4')
    end
    
    L  = Np/4;                      % number of overlap samples, it should be <= Np/4
    P  = pow2(nextpow2(fs/da/L)); % Eq (19), we can also get N = P*L
    N  = P*L;                       % observation time T in discrete domain
    ao = (-1:1/N:1).*fs;            % cyclic frequency (alpha)
    fo = ((-1/2):1/Np:(1/2)).*fs;   % spectral frequency (f)

    % Input vector processing, the length of input vector will be made to N
    if length(input)<N
        input(N) = 0;      % pad zeros
    else
        input = input(1:N); % truncate input to length N
    end

    % Implement Figure 3 in (1): Computation of the Complex Demodulates
    % Construct input matrix in Figure 3, call it x
    NN = fix((P-1)*L+Np);            % number of elements in x matrix
    xx = input;
    if length(xx)<NN
        xx(NN) = 0;             % pad zeros
    else
        xx = xx(1:NN);          % truncate input to length NN
    end
    x = zeros(Np,P);            % x matrix initialization
    for k = 0:P-1
        x(:,k+1) = xx(k*L+1:k*L+Np);
    end

    % Windowing
    a  = hamming(Np);        % Construct N'-point Hamming window
    XW = diag(a)*x;          % Output of N'-point Hamming window

    % Centered N'-point FFT (zero frequency in the 0 bin)
    XF1 = fftshift(fft(XW));
    XF1 = [XF1(:,P/2+1:P) XF1(:,1:P/2)];

    % Downshift in frequency
    E = zeros(Np,P);
    for k = -Np/2:Np/2-1    % length of l is Np
        for m = 0:P-1          % length of k is P
            E(k+Np/2+1,m+1) = exp(-1i*2*pi*m*L*k/Np);
        end
    end
    XD = XF1.*E;                 % Output of Figure 3


    % Implement Figure 5 in (1): Implementation of the SSCA
    % Replication, repeat the k-th column of XT (XT(:,k)) L times
    XR = zeros(Np,P*L);
    for k = 1:P
        XR(:,(k-1)*L+1:k*L) = XD(:,k)*ones(1,L);
    end

    % Multiplication
    % Convert input vector into a matrix, the vector first becomes a row
    % vector, and repeat Np times. xc is (Np,N), the same size as XR
    inputRep = ones(Np,1)*input';
    XM = conj((XR.*inputRep)');

    % Centered N-point FFT (zero frequency in the 0 bin)
    XF2 = fftshift(fft(XM));
    XF2 = [XF2(:,Np/2+1:Np) XF2(:,1:Np/2)]; % Output of Figure 5
    M   = XF2;

    % Convert M to Sx
    Sx = NaN(Np+1,2*N+1);
    for k1 = 1:N
        k2 = 1:Np;
        a = (k1-1)/N + (k2-1)/Np - 1;
        f = ((k2-1)/Np - (k1-1)/N) / 2;
        k = 1 + Np*(f+1/2);
        r = 1 + N*(a+1);
        for k2 = 1:Np
            Sx(round(k(k2)),round(r(k2))) = M(k1,k2);
        end
    end

end



