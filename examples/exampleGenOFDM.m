% Tampere University of Technology
%
% exampleGenCDMA.m
%
%   This script shows how to generate
%   cdma signals using the functions
%   developed. 
%
% --
% Pedro Figueiredo e Silva
% pedro.silva@tut.fi

close all;
clear variables;
addpath(genpath('../functions'));

Ns = 10;
N  = 1000;

% Generates OFDM signal
[symbol,ft] = genOFMD(N,Ns,2,'psk');

% Print
axs = createfig('on');
plot(axs,linspace((-20e6)/2,(20e6)/2,length(symbol)),fftshift(abs(fft(symbol))));
title('OFDM symbol');