
figidx=1;
[figs(figidx),h] = createfig('on');
nFrames = 20;
vidObj = VideoWriter('cdmasignals.avi');
vidObj.Quality = 100;
vidObj.FrameRate = 2;
open(vidObj);
for k=1:30;
    
    samples    = [                     %%% Matrix with (#DATA SYMBOLS, OVERSAMPLING)
        500, ceil(fs/frefCDMA),...     % CDMA: Input symbol vector length | oversampling
        randi([10 100],1,1);...        %  "" : Spreading factor randi(RANGE,Lines,Cols)
        ...
        200, ceil(fs/frefOFDM), NaN;...% OFDM: Input symbol vector lenght | oversampling
        ];
    
    modulation  = [                    %%% Matrix with (CONSTELLATION SIZE, MODULATION TYPE)
        2^6, {'qam'};...               % CDMA modulation constellation size
        2^2, {'psk'};...               % OFMD modulation constellation size
        ];
    samples(1,3);
    
    [cdma,spc] = genCDMA(samples(1,1),samples(1,2), samples(1,3), modulation{1,1},modulation{1,2});
    
    signalPSD = abs(fftshift(fft(xcorr(cdma))));
    plot(figs(figidx), linspace(-1/2,1/2,length(signalPSD)),signalPSD/max(abs(signalPSD)));
    title(figs(figidx),['CDMA SF = ',num2str(samples(1,3)),' SUM(SCODE) = ',num2str(sum(spc))]);
    ylabel('Amplitude (W/Hz)');
    xlabel('Normalised frequency');
    movegui(h,'onscreen');
    % datetick;
    drawnow
    pause(0.5);
    writeVideo(vidObj, getframe(h));
end
close(vidObj);