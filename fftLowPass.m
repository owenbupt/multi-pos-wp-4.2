function y = fftLowPass(x, fs, fcutoff, show)
% =========================================================================
% The sharpest :-) low pass filter based on fft and ifft operations
%
% Function y = fftLowPass(x, fs, fcutoff, show), 
% where 
%   x (respective y) is input (respective output) sequence,
%   fs is sampling frequency
%   fcutoff is the low pass filter cut off frequency 
%       (it must be lower than fs/2)
%   show determines whether the signal spectrums should be displayed (==1)
%       or not (~=1)
%
% Example
%   "x = randn(1,10000); fftLowPass(x, 100e6, 10e6, 1);"
%
% Note
%   Standard way how to implemented low pass filter (FIR) in Matlab could 
%   be as follows
%
%   % Make filter
%   % L = 256;
%   % f = [0 8e6 14e6 (fs./2)] ./ (fs./2);
%   % a = [0.995 0.995 0.001 0.001];
%   % B = remez(L,f,a);
%   % Show frequency characteristic
%   % freqz(B,1,512,fs,'whole');
%   % Filtration
%   % y = filter(B,1,x);
%
% Ondrej DANIEL, E465505
% 18.10.2011
% =========================================================================
    
    x = x.';

    % General definitions
    N = length(x);
    fAxis = ((0:N-1) - fix(N/2)) * (fs/N);

    % Spectrum calculation
    xS = fftshift(fft(x));

    % Find cutoff frequencies within frequency axis
    [~,p1] = min(abs(-fAxis-fcutoff));
    [~,p2] = min(abs(fAxis-fcutoff));

    % Prepare the frequency mask
    mask = zeros(1,N);
    mask(p1:p2) = 1;

    y = (ifft(fftshift(xS .* mask)));

    % Displaying results
    if (show == 1)   
        yS = fftshift(fft(y));      
        showMask = 20*log10(mask .* max(abs(yS)));
        showMask(isinf(showMask)) = 20*log10(abs(min(yS(abs(yS) > 0))));           
        figure; plot(fAxis, [20*log10(abs([xS; yS])); showMask].','-x');    
    end
    y = y(:);
    
end
% EOF