function [ axs, fig ] = createfig(visible)
%CREATEFIG creates a new figure and returns handles for it and its axes
%   A simple wraper to quickly obtain axes handles to a figure. The default
%   behaviour is set to "hold" all drawings in the axes.
%
%   USAGE
%   [ axs, fig ] = createfig({1|0})
%
%   INPUT
%   VISIBILITY - by default is set to 'off'. Set on to see the figure.
%
%   OUTPUT
%   AXS - Figure's axes
%   FIG - Figure's handle
%
%   NOTE
%   Use get(hAxs(k),'parent') to retrieve corresponding figure handle
%
%   Pedro Silva, Tampere University of Technology, 2013

    % Parameter check
    if ~exist('visible','var')
        visible = 'on';
    else
        if visible
            visible = 'on';
        else
            visible = 'off';
        end
    end
   
    % Creates handles
    fig     = figure('Visible',visible);
    axs     = axes('Parent',fig);
    hold(axs,'on');
    
end

