function [hAxs,hSubfig] = createhandles(opvector,visibility,type)
%CREATEHANDLES creates a vector with figure handles
%	If no input is passed, a single handle (visible) to a normal plot is
%	created.
%
%	USAGE
%   [hAxs,hSubfig] = createhandles(opvector,visibility,type)    
%
%   DESCRIPTION
%   The output vector is the same size as the output vector, however only
%   the positions marked as 1 will get an handle created. Type specifies if
%   the normal plot or the subplot is used.
%
%   INPUT
%   opvector   - Vector of options (mark with one to create an handle)
%   visibility - Sets the figure visibility
%   type       - Default is plot (1), however subplot (0) is also available
%
%   OUTPUT
%   hAxs       - axes handles  
%   hSubfig    - subplot handle
%
%   NOTE
%   Use get(hAxs(k),'parent') to retrieve corresponding figure handle
%   
%   AUTHOR
%   Pedro Silva
%   Tampere University of Technology, 2013
    
	
	% Default parameters 
	if isempty(opvector)
		opvector   = 1;
		visibility = 'visible';
		type       = 1;
    else
        hAxs   = opvector;    
    end
	
    hSubfig = [];
    if type % normal plot
        for k=1:length(opvector)
            if opvector(k)
                hAxs(k) = createfig(visibility);
            else
                continue;
            end
        end
    else % subplot
        m = round(sum(opvector)/2);
        n = 2;
        p = 0;
        for k=1:length(opvector)
            if opvector(k)
                p       = p+1;
                hAxs(k) = subplot(m,n,p);
                hold(hAxs(k),'on');
            else
                continue;
            end
        end
    end
end


