function ack = outputfig(filename, handles, format)
    %OUTPUTFIG saves the figure to the disk and exports it in PNG format
    %   By default, this function tries to save and export the figures given in
    %   the HANDLES vector, using FILENAME cell as the target. The format to be
    %   used can also be specified as an optional parameter.
    %   The output ACKs that all figures where succesfully saved.
    %
    %   DEPENDENCIES
    %   For better figure output (might be slower), please download the
    %   export_figure package present in file exchange.
    %   http://www.mathworks.com/matlabcentral/fileexchange/23629-exportfig
    %
    %   INPUT
    %   FILENAME - name to use when saving the figure to the disk
    %   FIG      - figure's handle
    %   [FORMAT] - default format is set to PNG
    %
    %   OUTPUT
    %   ACK      - signals if figure was saved with success
    %
    %   AUTHOR
    %   Pedro Silva
    %   Tampere University of Technology, 2013
    
    % Parameter check
    nn = size(filename,1);
    nf = size(handles,1);
    if nn~=nf
        appendnb = 1;
    else
        appendnb = 0;
    end
    
    % Default format (for export_fig)
    if ~exist('format','var')
        format = '-pdf';
    end
    
    
    % Assume it succeeds
    ack = 1;
    for k=1:max(size(handles))
        
        % Skip empty handles and sets name
        if handles(k)== 0, continue; end;
        if appendnb
            name = [filename,'_fig_',num2str(k,'%02d')];
        else
            if k==1
                name = filename;
            else
                name = filename{k};
            end
        end
        
        % If binary saving fails, skip all
        try 
            saveas(handles(k),name); % Save's .fig
            try % Might not be present
                beautifyfig(handles(k));
                export_fig(name,format,'-a4','-transparent','-m2','-cmyk',handles(k));
            catch % normal MATLAB command
                print(handles(k), '-dpng', name);
            end
        catch
            ack = 0;
        end
    end
    
end
