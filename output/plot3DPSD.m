function plot3DPSD(hFig,x,y,z,xtxt,ytxt,titletxt)
    mesh(hFig,x,y,z,'EdgeColor','k','edgealpha',0,'facealpha',0.7,'facecolor','interp');
    axis(hFig,'tight');
    xlabel(hFig,xtxt);
    ylabel(hFig,ytxt);
    title(hFig,titletxt);
    view(hFig,-108,58);    
    drawnow;
end
