function plotPSD(hFig,fs,data,gfxtitle)
 
    signalPSD = 10*log10( fftshift(abs(fft(xcorr(data))).^2));
%     signalPSD = abs(fftshift(fft(data)));
    plot(hFig, linspace(-fs/2,fs/2,length(signalPSD)),signalPSD);
    ylabel(hFig,'Amplitude (W/Hz)');
    xlabel(hFig,'Frequency (Hz)');    
    title(hFig,gfxtitle);
end