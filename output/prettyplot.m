function prettyplot(pdfig,x,y,note,ctag,lName)
    
    
    interpMin=min(x);
    interpMax=max(x);
    interpSteps=8000;
    
    if isempty(pdfig)
        pdfig = gca;
    end
    
    nbCases = size(y,1);
    for N = 1:nbCases
        hLine = plot(pdfig,x,y(N,:),ctag{N},'DisplayName',lName{N},'MarkerFaceColor',ctag{N}(1));
    end
    
    xx = linspace(interpMin,interpMax,interpSteps);
    for N = 1:nbCases
        hLine = plot(pdfig,xx,interp1(x,y(N,:),xx,'pchip'),ctag{N}(1));
        set(get(get(hLine,'Annotation'),'LegendInformation'),...
            'IconDisplayStyle','off'); % Exclude line from legend
    end
    grid(pdfig,'on');
    hLegend = legend(pdfig,cellstr(lName),'Location','NorthWest');
%     legend(hLegend,'Location','best')
    set(hLegend,'Interpreter','latex');
    ch=get(hLegend,'children');
    for k=1:length(ch)
        set(ch(k),'LineStyle','-');
    end
    
    hTit = title(pdfig,note);
    set(hTit,'Interpreter','latex');
    xlabel(pdfig,'SNR');
    ylabel(pdfig,'Probability');
%     if ~isempty(note)
%         delta = 0.08;
%         pos=get(hLegend,'position');
%         annotation('textbox',...
%         [pos(1) pos(2)-delta-0.01 pos(3) delta],...
%         'String',{note},...
%         'FontSize',12,...
%         'FontName','times new roman',...
%         'FontWeight','bold',...
%         'LineStyle','-',...
%         'EdgeColor',[0 0 0],...
%         'LineWidth',1,...
%         'BackgroundColor',[0.9 0.9 0.9],...
%         'HorizontalAlignment','center',...
%         'Color',[0 0 0]);
%     end
    try
        xlim(pdfig,[interpMin interpMax]);
    end
    ylim(pdfig,[0 1]);
    
end