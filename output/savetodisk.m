function savetodisk(name,sSt,seq)
%SAVETODISK is a function wraper for save
%   Stores information in the structure sSt to the file with the given
%   name. The fields input contains the desired fields to be saved
%
%   AUTHOR
%   Pedro Silva
%   Tampere University of Technology

   if ~seq
        % SAVE DATA
        save([name,...
            '.mat'],...
            '-struct',...
            'sSt','stat','sim','param');       
   else
        % SAVE DATA
        save([name,...
            '.SEQ',num2str(seq),...
            '.mat'],...
            '-struct',...
            'sSt','stat','sim','param');
   end
end

