function captureframe(vObj, hScreen)
%CAPTUREFRAME writes to a frame to the given video object
%
%   DESCRIPTION
%   The handle provided is captured as a frame
%
%   INPUT
%   vObj    - Video object
%   hScreen - Handle from where to take frame (default: gcf)
%
%   Pedro Silva
%   Tampere University of Technology, 2014

    %updates screen
    drawnow;

    %obtains frame
    if ~exist('hScreen','var') || isempty(hScreen)
        frame = getframe(gcf);
    else
        frame = getframe(gcf);
    end    
    writeVideo(vObj,frame);
end
