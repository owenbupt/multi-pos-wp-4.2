function tx = genericOFDM(nbSym,overSample,nbCarriers,gi,gBandLeft,gBandRight,modulation,pilotPower,pilotSpacing,pseq,BW)
%GENERICOFDM generates one or more OFDM symbols
%
%   DESCRIPTION
%   The function provides a sequence of OFDM symbols generically created
%   from the provided inputs.
%
%   tx = genericOFDM(nbSym,nbCarriers,gi,gBand,modulation,pilotPower,changingPilots)
%
%   In every case, pilots are used and their power can be controlled through the
%   PILOTPOWER variable. Note as well that the CHANGINGPILOTS flag allows
%   the random generator to produce a single sequence to apply in every
%   symbol, or a random sequence for every symbol. The later approaches
%   better the reality for some systems (if not all). However, it should
%   still be repeated after a few symbols.
%
%   INPUT
%   nbsym      - number of symbols to produce
%   nbCarriers - Number of carriers
%   GI         - Percentage of number of samples (<number carriers>/GI)
%   GBAND      - Guard band, vector with value for [Left, Right] bands
%   modulation - Size of constellation 2^N
%   pilotPower - Pilot power boost
%   pseq       - Set true for random pilots for every symbol
%
%   OUTPUT
%   tx         - nbsym OFDM symbols (sequence)
%
%   AUTHOR
%   Pedro Silva
%
%   Based on a script by
%   Ali Hazmi, ali.hazmi@tut.fi
    
    % Inputs
    N           = nbCarriers;                      % number of carriers
    GI          = round(nbCarriers/gi);            % Guard interval in samples
    Nc          = nbCarriers-gBandLeft-gBandRight; % data carriers + pilots
    
    pilotPos                    = zeros(1,Nc);
    pilotPos(1:pilotSpacing:Nc) = 1;
    Np                          = length(1:pilotSpacing:Nc);%+ 1;
    Nu                          = Nc-Np; % active carriers = data carriers - pilot carriers
    
    
    % pilots with random BPSK data
    if pseq
        pilotVal = randsrc(Np,nbSym,-1:2:1,18723)*pilotPower;
    else
        %TODO to make it more accurate, do a big matrix and remove windows
        pilotVal = kron(randsrc(Np,1,-1:2:1,18723)*pilotPower,ones(1,nbSym));
    end

    % Generate symbols
    tx_qam = qammod(randsrc(Nu,nbSym,0:modulation-1),modulation,[],'gray');
    if modulation == 64
        tx_qam = tx_qam/sqrt(42);
    elseif modulation == 16
        tx_qam = tx_qam/sqrt(10);
    else     
        for k = 1:nbSym
            tx_qam(:,k) = tx_qam(:,k)/std(tx_qam(:,k));
        end
    end
    tx_OFDM_sym(N -(gBandLeft+gBandRight),nbSym) = 0; % intialize the OFDM symbol useful part
    
    % Create OFDM symbols
    tx_OFDM_sym(pilotPos > (1-eps),:)  = pilotVal;
    tx_OFDM_sym(pilotPos < (0+eps),:)  = tx_qam;
    tx_OFDM_sym_freq                   = [zeros(gBandLeft,nbSym);tx_OFDM_sym;zeros(gBandRight,nbSym)];
%     tx_OFDM_sym_freq                   = [zeros(gBandLeft,nbSym);tx_OFDM_sym;zeros(gBandRight,nbSym)];
    
    % Oversample
    L = size(tx_OFDM_sym_freq,1);
%     tx_OFDM_sym_freq_zpad = zeros(BW-N,nbSym);
    tx_OFDM_sym_freq_zpad                  = zeros(N*overSample,nbSym);
    tx_OFDM_sym_freq_zpad(1:L/2,:)         = tx_OFDM_sym_freq(L/2+1:end,:);
    tx_OFDM_sym_freq_zpad(end-L/2+1:end,:) = tx_OFDM_sym_freq(1:L/2,:); 
        
%     tx_OFDM_sym_time = tx_OFDM_sym_freq; %Allocate
    for k=1:nbSym
        tx_OFDM_sym_time(:,k)          =  sqrt(length(tx_OFDM_sym_freq_zpad))*2.45*ifft(tx_OFDM_sym_freq_zpad(:,k),length(tx_OFDM_sym_freq_zpad(:,k)));
    end
    tx_OFDM_sym_time_GI                = [tx_OFDM_sym_time(end-GI+1:end,:); tx_OFDM_sym_time(:,:)];
    tx                                 = tx_OFDM_sym_time_GI(:);
    
end

% 
% function tx = genericOFDM(nbSym,overSample,nbCarriers,gi,gBandLeft,gBandRight,modulation,pilotPower,pilotSpacing,pseq)
% 
% 
% %TO CHANGE
%     dataCarriers = nbCarriers-gBandLeft-gBandRight;
%     nbCarriers   = nbCarriers;
%     BW           = 20e6;
%     
%     % Defaults
%     if ~exist('sgPower','var') || isempty(sgPower)
%         sgPower = 1;
%     else
%         assert(sgPower>0);
%     end
%     
%     if ~exist('modSelect','var') || isempty(modSelect)
%         modSelect = 'psk';
%     end
%     
%     if ~exist('txBits','var') || isempty(txBits)
%         [E,F] = log2(nbCarriers);
%         if E==0.5 %see help log2
%             data = randsrc(1,nbCarriers,0:modulation-1);
%         else
%             nbCarriers = 2^(F+1);
%             data = randsrc(1,nbCarriers,0:modulation-1);
%         end
%     end
%     
%     if ~exist('Ts','var')
%         Ts = 1/(BW);
%     end
%     
%     
%     % Encoding
% %     if strcmpi(modSelect,'qam')
%         mdata = qammod(data,modulation,[],'gray');
% %         if modulation == 64
% %             kmod = 1/sqrt(42);
% %         elseif modulation == 16
% %             kmod = 1/sqrt(10);
% %         else
% %             kmod = 1;
% %         end
% %     elseif strcmpi(modSelect,'psk')
% %         mdata = psk(data,modulation);
% %         if modulation == 4
% %             kmod = 1/sqrt(2);
% %         else
% %             kmod = 1;
% %         end
% %     end
% %     
%     %%% Generalise after validated
%     %Map to symbol
%     d      = mdata(1:dataCarriers)';
%     dF     = BW/nbCarriers;       % TFFT = 1/dF
%     Tgi    = 1/(dF)/4; % TFFT/4
%     
%     % Equation (18-21)
% %     Tsym   = 4e-06*overSample;
% %     n      = 1; %nSym;
% %     k      = (-26:26)';
% % %     window = [0.5;ones(80,1);0.5];
% %     
% %     tx     = zeros(round(Tsym),1);
% %     for Td = 0:round(Tsym)
% %         tx(Td+1,1) = sum(d.*exp(1i*2*pi.*getFreqOffset().*dF.*(Td.*Ts-Tgi))) ...
% %             + getPilotSignal(n).*sum(getPilot().*exp(1i.*2.*pi.*k.*dF.*(Td.*Ts-Tgi)));
% %     end
% %     
%     % Padding with zeros results in oversampling
% %     if overSample > 1
% %         zeroint = zeros(length(tx)*overSample,1);
% %         zeroint(1:40) = tx(1:40);
% %         zeroint(end-40:end) = tx(41:end);
% %         tx = zeroint;
% %     end
%     
% %     if overSample > 1
% % %         zeroint = zeros(length(tx)*overSample,1);
% % %         zeroint(1:40) = tx(1:40);
% % %         zeroint(end-40:end) = tx(41:end);
% %         tx = [tx;zeros(length(tx)*overSample,1)];
% %     end
%     
%     
% %     % Alternative implementation (should be faster)
% %     d          = mdata(1:64)';
% %     fftlen     = length(d)*overSample;
% %     fast_tx    = sqrt(fftlen)*ifft(d(:),fftlen);
% %     dc         = round(fftlen/2);
% %     pilotFreq  = [0 -7 7 21 -21];
% %     pilotValue = [0 1  1  1  -1];
% %     
% %     fast_tx(dc+pilotFreq) = pilotValue;
% % %     fast_tx    = insertGuardInterval(fast_tx,length(fast_tx),64/4*overSample,1);
% %     tx    = insertGuardInterval(fast_tx,64/4,1);
%    
% %     fast_tx=[fast_tx;zeros(length(tx)*overSample,1)];
%     
%     %Apply power
% %     tx      = sqrt(sgPower)*tx;
% %     fast_tx = fast_tx;
%     
% end
% 
% function symbolGI = insertGuardInterval(symbol,lenGI,type)
%     %TXGI insert a guard interval in the given symbol
%     %
%     %   INPUT
%     %   symbol    - Data symbol
%     %   lenSymbol - Lenght of the symbol (OFDM: NbCarriers)
%     %   lenGI     - Lenght of the guard interval
%     %   type      - Choose between Cyclic Prefix or 0 guard interval
%     %
%     %   OUTPUT
%     %   symbolGI  - Symbol with guard interval inserted
%     %
%     %   Pedro Silva, Tampere University of Technology, 2013
%     
%     if type == 1
%         symbolGI = [symbol(end-lenGI:end);symbol];
%     else
%         symbolGI = [zeros(lenGI,1);symbol];
%     end
% end
% 
% 
% function data = psk(data,N)
%     %PSK maps the input symbols to N chips
%     %   USAGE
%     %   data = psk(data,N)
%     %
%     %   INPUT
%     %   data - symbol stream
%     %   N    - constellation mapping size
%     %
%     %   OUTPUT
%     %   data - BPSK or DQPSK modulated signal
%     %
%     %   Pedro Silva, Tampere university of Technology, 2013
%     
%     
%     if N == 2 % QPSK
%         data(data==0) = -1;
%         
%     elseif N == 4 % DQPSK
%         data(data==0) = -1 - 1i;
%         data(data==2) =  1 - 1i;
%         data(data==1) = -1 + 1i;
%         data(data==3) =  1 + 1i;
%     else
%         error('Available: BPSK and DPSK');
%     end
%     
% end
% 
% 
% function fidx = getFreqOffset()
%     %getFreqOffset returns the frequency offsets for the data symbols
%     %
%     %   In the standard, this is the mapping function presented in 18.3.5.10,
%     %   but instead of using K, this function returns all the frequency offsets
%     %   in a vector.
%     %
%     %   OUTPUT
%     %   fidx - Frequency offsets (48x1 column vector)
%     %
%     %   Pedro Silva, Tampere University of Technology, 2013
%     
%     
%     fidx        = (0:48-1)';
%     fidx(1:5)   = fidx(1:5)-26;
%     fidx(6:18)  = fidx(6:18)-25;
%     fidx(19:24) = fidx(19:24)-24;
%     fidx(25:30) = fidx(25:30)-23;
%     fidx(31:43) = fidx(31:43)-22;
%     fidx(44:48) = fidx(44:48)-21;
%     
% end
% 
% 
% function polarity = getPilotSignal(symbolSeqNumber)
%     %GETPILOTSIGNAL returns the pilot polarity for symbol N
%     %
%     %   USAGE
%     %   signal = getPilotSignal(0) %for OFDM signal polarity
%     %   singal = getPilotSingal(N) %for OFDM data polariity (N>0)
%     %
%     %   Notice that the first entry (0) is mapped to the first entry in the
%     %   MATLAB vector (vec(1)).
%     %
%     %   !!!
%     %   This entry is the OFDM SIGNAL polarity and to retrieve it, the sequence
%     %   number should be used.
%     %   !!!
%     %
%     %   The function uses N+1 to access the internal polarity vector
%     %
%     %   INPUT
%     %   N        - Symbol sequence number
%     %
%     %   OUTPUT
%     %   polarity - Polarity of pilot data for given symbol sequence number
%     %
%     %   Pedro Silva, Tampere University of Technology, 2013
%     
%     % Polarity of the pilot sequence
%     p = [ 1, 1, 1, 1,-1,-1,-1, 1,...
%         -1,-1,-1,-1, 1, 1,-1, 1,...
%         -1,-1, 1, 1,-1, 1, 1,-1,...
%         1, 1, 1, 1, 1, 1,-1, 1,...
%         1, 1,-1, 1, 1,-1,-1, 1,...
%         1, 1,-1, 1,-1,-1,-1, 1,...
%         -1, 1,-1,-1, 1,-1,-1, 1,...
%         1, 1, 1, 1,-1,-1, 1, 1,...
%         -1,-1, 1,-1, 1,-1, 1, 1,...
%         -1,-1,-1, 1, 1,-1,-1,-1,...
%         -1, 1,-1,-1, 1,-1, 1, 1,...
%         1, 1,-1, 1,-1, 1,-1, 1,...
%         -1,-1,-1,-1,-1, 1,-1, 1,...
%         1,-1, 1,-1, 1, 1, 1,-1,...
%         -1, 1,-1,-1,-1, 1, 1, 1,...
%         -1,-1,-1,-1,-1,-1,-1];
%     
%     polarity = p(symbolSeqNumber+1);
%     
% end
% 
% function pilot = getPilot()
%     %GETPILOT returns the pilot sequence for OFDM data signal
%     %
%     %   OUTPUT
%     %   pilot - Vector containing pilot sequence (Nx1)
%     %
%     %   Pedro Silva, Tampere University of Technology, 2013
%     
%     pilot     = zeros(53,1);
%     pilot(6)  = +1;
%     pilot(20) = +1;
%     pilot(34) = +1;
%     pilot(48) = -1;
%     
%     %     % Check where the pilots are being placed
%     %     k=-26:26
%     %     k(abs(pilot)==1)
% end
% 
% 
