
function [v, fid] = sequential_read_complex_binary(fid, filename, count)

% usage: read_complex_binary (filename, [count])
%
%  open filename and return the contents as a column vector,
%  treating them as 32 bit complex numbers
%
    
    if ~exist('fid','var') || isempty(fid) || isnan(fid)
        fid = fopen (filename, 'rb');
    end
    
    if (fid < 0)
        v = 0;
    else
        t = fread (fid, [2, count], 'float');
        v = t(1,:) + t(2,:)*1i;
        [r, c] = size (v);
        v = reshape (v, c, r);
    end
    
end