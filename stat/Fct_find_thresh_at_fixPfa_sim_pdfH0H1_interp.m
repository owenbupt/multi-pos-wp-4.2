function [thresh_gamma, err_pfa, Pd, xaxH0, cdfH0, xaxH1, cdfH1]=Fct_find_thresh_at_fixPfa_sim_pdfH0H1_interp(zH1, zH0, fixed_Pfa);
    %compute the required threshold and the corresponding Pd in order
    %to get a fixed Pfa.
    %zH1=the decision statistic when H1:signal+noise is true
    % zH0=the decision statistic when 0: noise only is true
    %fixed_Pfa =false alarm probability
    %thresh_gamma=decision threshol
    %err_pfa= error in reaching the pfa (if too large => Pd is far from the
    %true value)
    %Pd= detection probability
    %xaxH0= x axis in plotting the CDF for H0 hypothesis
    %cdfH0 =interpolated CDF under H0 hypothesis
    %xaxH1= x axis in plotting the CDF for H1 hypothesis
    %cdfH1 =interpolated CDF under H1 hypothesis
    %
    %created on 6th October 2004, by Simona
    %updated 13th of June 2013, Simona
    %@TUT
    
    %choose the number of bins in building pdf
    bins=20;
    %build the histograms in correct and incorrect windows
    temp=abs(zH1(:));
    [histo_correct,xaxH1]=hist(temp,bins);
    pdfH1=histo_correct/sum(histo_correct);
    cdfH1=zeros(1,length(xaxH1));
    for xx=1:length(xaxH1),
        cdfH1(xx)=sum(pdfH1(1:xx));
    end;
    
    temp2=abs(zH0(:));
    [histo_incorrect,xaxH0]=hist(temp2,bins);
    pdfH0=histo_incorrect/sum(histo_incorrect);
    cdfH0=zeros(1,length(xaxH0));
    for xx=1:length(xaxH0),
        cdfH0(xx)=sum(pdfH0(1:xx));
    end;
    
    
    %added Oct 2013
    %because xaxH0 and xaxH1 are not overlapping, we interpolate in order to
    %get overlapping curves
    xax_min=min([xaxH0 xaxH1]);
    xax_max=max([xaxH0 xaxH1]);
    stepi=(xax_max-xax_min)/8000; %interpolate with 8000 points
    xax_common=[xax_min:stepi:xax_max];
    methodi='linear';
    %introduce first and last points in original axes
    xaxH01=xaxH0; cdfH01=cdfH0;
    if xaxH0(1)>xax_common(1),
        xaxH01=[xax_common(1) xaxH0];
        cdfH01=[0 cdfH01];
    end;
    if xaxH0(end)<xax_common(end),
        xaxH01=[xaxH01 xax_common(end)];
        cdfH01=[cdfH01 1];
    end;
    
    xaxH11=xaxH1; cdfH11=cdfH1;
    if xaxH1(1)>xax_common(1),
        xaxH11=[xax_common(1) xaxH1];
        cdfH11=[0 cdfH11];
    end;
    if xaxH1(end)<xax_common(end),
        xaxH11=[xaxH11 xax_common(end)];
        cdfH11=[cdfH11 1];
    end;
    
    cdfH0_interp=interp1(xaxH01, cdfH01, xax_common, methodi);
    cdfH1_interp=interp1(xaxH11, cdfH11, xax_common, methodi);
    
    [err_pfa, min_pos]=min(abs(1-cdfH0_interp-fixed_Pfa));
    thresh_gamma=xax_common(min_pos);
    %find the nearest threshold from the correct window,
    %corresponding to the calculated threshold
    [~, min_posPd]=min(abs(xax_common-thresh_gamma));
    Pd=cdfH1_interp(min_posPd);

    %
    cdfH0=cdfH0_interp;
    cdfH1=cdfH1_interp;
    xaxH0=xax_common;
    xaxH1=xax_common;
    
    end
    