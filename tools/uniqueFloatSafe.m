function outArray = uniqueFloatSafe(inArray)

    md = 10000000;
    
    outArray = unique(round(inArray*md)/md);


end