function [scf,caf,fax,alpha] = analyseSpectrum(opcontrol,param,signal)
%ANALYSESPECTRUM performs several spectral analysis
%
%   DESCRIPTION
%   This function executes one or several spectral analysis methods in
%   other to obtain an SCF/CAF.
%   The output is always put to a single variable
%
%   INPUT
%   opcontrol - vector with methods to run
%   param     - structure with parameters
%   signal    - signal to use
%
%   OUTPUT
%   The output is always (f,a)!
%   SCF, CAF, FAX, ALPHA
%
%   Pedro Silva
%   Tampere University of Technology, 2014
    

    % Retrieve parameters
    fs         = param.fs;
    windowCtl  = param.windowType;
    alphas     = param.alphaLags;
    lenFFT     = param.lenFFT;
    deltaFreq  = param.deltaFreq;
    limFreq    = param.limFreq;
    deltaAlpha = param.deltaAlpha;
    limAlpha   = param.limAlpha;
    
    scf=[]; caf=[]; fax=[]; alpha=[];
    
    % Gets pointer to operation control
    execidx = opcontrol(end-1);
    
    % Symbol rate estimation
    if opcontrol(execidx)
        [a,J,alpha,r] = symrate_fft2(signal,1,-20:20,1e-04);
    end
    
    % Estimation Cyclic Autocorrelation Function (CAF) and its conjugate (CCAF)
    execidx = execidx + 1;
    if opcontrol(execidx)
        [CAF, CCAF, taxCAF] = Fct_CAF_and_CCAF_sim(fs,alphas, signal, windowCtl, lenFFT);
        caf=CCAF;alpha=taxCAF;
    end
    
    % Estimation Spectral Correlation Function (SCF) with FFT method
    execidx = execidx + 1;
    if opcontrol(execidx)
        Nblocks          = fix(length(signal)/lenFFT);
        [cafFFT, scfFFT,~,faxFFT] = Fct_SCFviaFFT_fin9Apr13(fs, alphas, signal,windowCtl, lenFFT, Nblocks);
        scf              = scfFFT;
        caf              = cafFFT;
        alpha            = alphas;
        fax              = faxFFT;
    end
    
    % Estimation Spectral Correlation Function (SCF) with FAM method
    execidx = execidx + 1;
    if opcontrol(execidx)
        [~, famSCF, faxFAM, alphaFAM] = Fct_FAM_SCF_fin5Feb14(fs, signal, deltaFreq, deltaAlpha, limFreq, limAlpha);
        scf   = famSCF;
        fax   = faxFAM;
        alpha = alphaFAM;
        
%         [Sxp,fo,alphao] = WiFi_autofam(signal',fs,fs/128,fs/256,80,44,.55,256);
%         scf   = Sxp;
%         fax   = fo;
%         alpha = alphao;
        
    end
    
    
    % Estimation using the Strip Spectral Correlation Algorithm (SSCA)
    execidx = execidx + 1;
    if opcontrol(execidx)    
        [scf,alpha,fax] = ssca(signal, fs, deltaFreq, deltaAlpha);
    end
    
    
       
end