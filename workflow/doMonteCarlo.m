function figHandles = doMonteCarlo(sSt,online,plotfpa,plotfsnr,visibility)
    %DOMONTECARLO
    %
    %   Load monte carlo data and plot a comparison
    %
    %   %TODO
    %   Save images to disk
    %
    %   Author
    %   Pedro Silva, 2013
    %   Tampere University of Technology
    
    % Close older or existing figures
    close all;
    
    nbFreq                     = length(sSt.stat.cycFreq);
    pfa                        = sSt.sim.fixedPfa;
    figCount                   = 0;
    
    if ~exist('visibility','var') ||isempty(visibility)
        visibility             = 1;
    end
    
    % Fixed pfa
    if plotfpa
        if ~online
            vCNR                       = sSt.sim.cnr;
            probDetection              = NaN(length(vCNR),nbFreq);
            figHandles(length(vCNR),1) = 0;
            
            for iCNR = 1:length(vCNR);
                sCyclo = load([sSt.sim.folderData,sSt.sim.nameData,'.',sSt.sim.runDate,'#',num2str(sSt.sim.iterations),'$',num2str(sSt.sim.signals(1)),num2str(sSt.sim.signals(2)),num2str(sSt.sim.signals(3)),'@',num2str(vCNR(iCNR)),'.mat']); %
                sCyclo = sCyclo.stat;
                CNR    = vCNR(iCNR);
                
                % Calculate CDF
                [pd,figCount,figHandles] = plotCDF(sCyclo, pfa, CNR,[],figCount,figHandles,nbFreq,visibility);
                probDetection(iCNR,:) = pd;
            end
            
            % Plot the Pd plots
            strlegend            = [];
            cmap                 = colormap;
            figCount             = figCount+1;
            figHandles(figCount) = createfig(1);
            for k=1:nbFreq
                plot(figHandles(figCount), vCNR, probDetection(:,k),'linewidth',3,'color',cmap(20+(2^(k+2)+10*(k+2))+1,:));
            end
%             legend('Location','Best','\alpha_0','\alpha_1','\alpha_2');
            title(['Probability of detection for P_{FA}=', num2str(pfa)]);
            xlabel 'CNR';
            ylabel 'Pd';
            
        else
            figHandles = 0;
            sCyclo     = sSt.stat;
            CNR        = sSt.stat.cnr;
            vCNR       = CNR;
            % Calculate CDF
            probDetection = plotCDF(sCyclo, pfa, CNR,[],figCount,figHandles,nbFreq);
            disp(['Probability of detection ',num2str(probDetection)]);
        end
    end
    
    % Loop Cyclic Frequency
    if plotfsnr
        vCNR      = sSt.sim.cnr;
        for iFreq = 1:nbFreq
            % Loop CNR
            for iCNR = 1:length(vCNR);
                sCyclo = load([sSt.sim.folderData,sSt.sim.nameData,'.',sSt.sim.runDate,'#',num2str(sSt.sim.iterations),'$',num2str(sSt.sim.signals(1)),num2str(sSt.sim.signals(2)),num2str(sSt.sim.signals(3)),'@',num2str(vCNR(iCNR)),'.mat']); %
                sCyclo = sCyclo.stat;
                signal = sCyclo.energySignal(:,iFreq);      % Do not normalise!
                noise  = sCyclo.energyNoise(:,iFreq);
                CNR    = vCNR(iCNR);
                % Loop Pfa
                figCount             = figCount+1;
                figHandles(figCount) = createfig(1);
                pdPfa                = zeros(length(sSt.sim.itPfa),1);
                for iPfa=1:length(sSt.sim.itPfa)
                    pdPfa(iPfa) = getProbDetection(signal,noise,sSt.sim.itPfa(iPfa));
                end
                plot(figHandles(figCount),sSt.sim.itPfa,pdPfa);
                title(['Pd Vs Pfa (',num2str(CNR),'dB , \alpha= ',num2str(iFreq-1),')']);
                xlabel 'P_{fa}'
                ylabel 'P_{d}'
            end
        end
    end
end







function [Pd,figCount,figHandles] = plotCDF(sCyclo, pfa, CNR, style, figCount, figHandles,nbFreq,visibility)
    
    % Veriy that we are using data for this CNR
    try
        assert(CNR == sCyclo.cnr);
    catch
        disp(CNR);
        disp(sCyclo.cnr);
        error('CNR does not match');
    end
    
    figCount             = figCount+1;
    figHandles(figCount) = createfig(visibility);
    cmap                 = colormap;
    Pd = zeros(1,nbFreq);
    for k = 1:nbFreq;
        
        signal = sCyclo.energySignal(:,k);      % Do not normalise!
        noise  = sCyclo.energyNoise(:,k);
        
        [Pd(k),axh0,axh1,h0,h1,vth] = getProbDetection(signal,noise,pfa);
%         [Pd(k),axh0,axh1,h0,h1] = getMyProbDetection(signal,noise,pfa);
        
        plot(figHandles(figCount),axh1,h1,'color',cmap(20+(2^k+10*k)+1,:),'Linewidth',2,'LineStyle','-');
        plot(figHandles(figCount),axh0,h0,'color',cmap(20+(2^(k+2)+10*(k+2))+1,:),'Linewidth',1,'LineStyle','--');
        
%         title(['CDF with noise at ',num2str(CNR),'dB ','(\alpha = ',num2str(k-1),')']);
        legend('Location','Best','H1','H0')
        ylabel 'F(x)'
        xlabel 'x'
%         ylim([0,1.1]);
                plot(figHandles(figCount),[0 axh1(end-1)],[1-pfa 1-pfa],'color',[0 0 0],'Linewidth',1.5,'LineStyle','-');
                plot(figHandles(figCount),[vth vth],[0 1],'color',[0 0 0],'Linewidth',1.5,'LineStyle','--');
        
    end
    title(['CDF with noise @ ',num2str(CNR),'dB ']);
%     legend('Location','Best','H1','H0')
    ylabel 'F(x)'
    xlabel 'x'
    ylim([0,1.1]);
    disp(['MC with CNR = ',num2str(CNR)]);
    try
    disp(['Values for (0,0): Pd = ',num2str(Pd(1))]);%,' Vth = ',num2str(thresh_gamma(1))]);
%     disp(['Values for (0,1): Pd = ',num2str(Pd(2))]);%,' Vth = ',num2str(thresh_gamma(2))]);
%     disp(['Improvement in Pd = ',num2str((Pd(2)-Pd(1))*100),' %'])
    end
end


function [pd,axh0,axh1,h0,h1,vth,epfa] = getProbDetection(signal,noise,pfa)
    
    [vth,epfa,Pnd,axh0, h0, axh1, h1] = ...
        Fct_find_thresh_at_fixPfa_sim_pdfH0H1_interp(signal,noise,pfa);
    pd = 1-Pnd;
    
    if epfa > 1
        warning(['Poor Pfa aproximation (err=',num2str(epfa),')']);
    end
    
end

%
function [pd,axh0,axh1,h0,h1,vth,epfa] = getMyProbDetection(signal,noise,pfa)
    epfa      = 0;
    [h1,axh1] = mycdfplot(signal);
    [h0,axh0] = mycdfplot(noise);
    [~,xpfa]  = min(abs( 1 - h0 - pfa));
    vth       = axh0(xpfa);
    [~,p]     = min(abs(axh1 - vth));
    pd        = 1-h1(p);
end
