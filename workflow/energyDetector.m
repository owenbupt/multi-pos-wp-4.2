function [ energyH0, energyH1 ] = energyDetector(txsignal,txnoise,fs)
    %energyDetector simple energy detector
    %   Computes the signal energy for H0 and H1 hypothesis
    
    energyH1 = computenergy(txsignal,fs);
    energyH0 = computenergy(txnoise,fs);
    
end

function energy = computenergy(signal,fs)
    
    energy = abs(signal).^2;
    energy = (1/length(signal)).*sum(energy.^2);
    
end


