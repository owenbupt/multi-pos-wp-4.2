function sParameters = getParameters()
%GETPARAMETERS returns a structure with all the necessary parameters
%   
%   This function contains all the parameters necessary to run the rflab.
%
%   INPUT
%   verbose     - Prints (several levels) or hides information
%
%   OUTPUT
%   sParameters - Contains several child parameter structures
%
%   Pedro Silva, Tampere University of Technology, 2013
    
    %%% Path requirements
    folderFigures = './figures/';
    folderData    = './rfdata/';
    if ~exist(folderFigures,'dir'), mkdir(folderFigures); end;
    if ~exist(folderData,'dir'), mkdir(folderData); end;
    
 
    %%% Simulation parameters
    runTAG      = 'forhist';
    saveData    = 1;                     % Saves mat file with SCF data
    numSim      = 1;                  % Number of observations
    saveEpoch   = randsrc(1,1,1:numSim); % Saves figures from a random observation
    saveFigures = 0;                     % Enables figures to be saved
    splitSave   = 0;                     % Used as counter for sequential file writing (set to 1 to start)
    showFigures = 0;                     % Display figures
    plotType    = 1;                     % 0: subplot, 1: plot
    recordVideo = 0;                     % Record video
    holdfig     = 0;                     % Activate (1) hold in axis
    plot3d      = 0;                     % Flag to force 3D plot off
    plot2d      = 1;                     % Flag to plot 2D cuts
    
    % FREQ parameters    
    simTime    = 2e-03;                  % Simulation or observation time
    fref       = 1e6;                    % Reference frequency (for normalisation)
    fs         = 40e6;                   % Sampling frequency should be high enough
    fpass      = 9e6;                    % Frequency passband
    fstop      = 11e6;                   % Frequency stopband
    Astop      = 20;                     % Stopband attenuation in dB
   
    lenFFT     = 1024;                     % Increasing this leads to a smaller deltaFreq!
    dAlpha     = 1e-5;%0.75e-5;
    dFreq      = 0.1;%0.075;
    windowing  = 3;                      % For FFT based computation (1 = None, 2 = Hanning; 3 = Hamming)
    cycFreq    = [];                     % Cyclic frequencies to be used
   
        
    decimate_input  = 0;
    if decimate_input  == 1
        decimate_factor = 7;
    else
        decimate_factor = 1;
    end
    
    % FOR REAL DATA ONLY
    source_input    = 0;
    source_path     = 'C:\Users\figueire\Local\worktmp\GnuRadio\Temp\';
    source_files    = dir([source_path,'*.d']);
    source_nbFiles  = size(source_files,1);
    %%% source_fs %%%% SET BY FILE NAME
    source_obsTime  = simTime;
    source_nbObs    = 1;
    source_chfreq   = [2412;2422;2437];
    source_vth      = 0.018;
    if source_input == 1
        snrVal =   88; %force value
    end

    
    %%%% SIMULATION only
    snrVal      = -5; %in dB                
    fixedPfa    = 0.1;
    itPfa       = linspace(0.01,1,100);
    
    
    % Signals in the mix
    % CDMA
    cdma_input    = 1;                    % Activate (1) CDMA signal
    cdma_fref     = 11e6;                 % CDMA: Signal reference frequency
    cdma_mod      = 4;                   % Modulation order
    cdma_mod_tp   = 0;                    % Modulation type (0:qpsk,1:qam)
    cdma_fc       = 11e6;                 % Chip rate
    cdma_fb       = 2e6;                  % Binary rate
    cdma_spfact   = 11;%11;               % Spreading factor
    cdma_nc       = ceil(simTime*cdma_fc/cdma_spfact); % Number of code epochs
    cdma_theocycl = cdma_fc;              % Theorectical period of cf
    cdma_symbrate = cdma_fc/(cdma_spfact*1e6); % symbol rate
    cdma_stem     = 0;                    % Display cyc. frequencies 
    
    
    % OFDM
    ofdm_input    = 1;                    % Activate (1) OFDM signal
    ofdm_fref     = 10e6;                 % OFDM: Signal reference frequency
    ofdm_Ntotal   = 64;                   % Number of carriers
    ofdm_Ndata    = 48; % 4,8, 16, 32     % Guard interval factor NFFT/gi
    ofdm_Npilot   = 4;
    ofdm_Nsub     = ofdm_Ndata + ofdm_Npilot;
    ofdm_bw       = 20e6;                 % OFDM bandwith
    ofdm_ppower   = 3;                   % Pilot power boost
%     ofdm_pspace   = 7;                    % Pilot spacing
    ofdm_mod      = 16;                   % Modulation
    ofdm_mod_tp   = 1;                    % Modulation type (forced to QAM)
    
    ofdm_deltaF   = ofdm_bw/ofdm_Ntotal;  
    ofdm_Tfft     = 1/ofdm_deltaF;
    ofdm_Tgi      = ofdm_Tfft/4;
    ofdm_sybtime  = ofdm_Tfft+ofdm_Tgi;    
    
    ofdm_symbols  = fix(simTime/ofdm_sybtime); % Number of OFDM symbols
    ofdm_theocycl = ofdm_sybtime;         % Theorectical period of cyc. frequencies
    ofdm_stem     = 1;                    % Display cyc. frequencies 
    
    % Noise
    noise_input = 1;                      % Enable noise
    
    %%% Execution flags
    cdma_plot  = 1;                       % plot CDMA
    ofdm_plot  = 1;                       % plot OFDM
    mix_plot   = 1;                       % plot mixed signal
    noise_plot = 0;                       % plot noise PSD
    noise_scf  = 0;                       % plot noise SCF  
    
    exec_scf_noise = 1;
    exec_edet  = 0;                       % Run energy detector
    exec_cyclo = 1;                       % Run cyclostationary
    exec_sym   = 0;                       % Run symbol rate estimation
    exec_caf   = 0;                       % Run CAF estimator
    exec_fft   = 0;                       % Run FFT method
    exec_fam   = 1;                       % Run FAM method
    exec_ssca  = 0;                       % Run SSCA method
    

    
    %%%%%%%%%%%%%
    % You should not change anything below this point, unless you know what
    % you're doing
    %%%%%%%%%%%%%
    
    
    %%% CTRL parameters
    signals = [                           %%% Controls which signals are generated and added together
        cdma_input;...                    % include cdma
        ofdm_input;...                    % include ofdm
        noise_input;...                   % include noise !! Always leave noise in the end!!
        source_input...
        ];
    
    inputs = [                              %%% Matrix with (#DATA SYMBOLS, OVERSAMPLING)
        cdma_fb*simTime, cdma_nc,...        % CDMA: Input symbol vector length | oversampling
        cdma_spfact, cdma_mod,...           %  "" : Spreading factor randi(RANGE,Lines,Cols)
        cdma_mod_tp, cdma_fc,...
        fs,NaN, cdma_symbrate,...
        cdma_theocycl;...                   %KEEP AS LAST ONE                
        ofdm_symbols, ofdm_Ntotal,...
        ofdm_Ndata, ...
        ofdm_deltaF, ofdm_ppower,...
        ofdm_Nsub, ...
        ofdm_mod,...
        ofdm_mod_tp,...
        ofdm_sybtime,...
        ofdm_theocycl,... %KEEP AS LAST ONE
        ];
    
    exec   = [                         %%% Controls which methods are used to estimate CAF/SCF
        exec_edet;...                  % 1 - Energy detector
        exec_cyclo;...                 % 2 - Cyclostationarity
        exec_scf_noise;...             % 3 - Perform noise analysis
        exec_sym;...                   % 4 - Symbol Rate
        exec_caf;...                   % 5 - CAF and CCAF
        exec_fft;...                   % 6 - SCF FFT (SIMONA)
        exec_fam;...                   % 7 - SCF FAM (SIMONA)
        exec_ssca;...                  % 8 - SCF SSCA (EVANDRO | MATLAB | MINE)
        ...                            % ...
        4;...                          % end-1 - Index for function keep this index updated!!!! 
        saveData;...                   % end - save data for HP testing
        ];
    
    plotv = [                            %%% Enabler for plots
        noise_plot;...                                        % 1  - Plot Noise PSD
        (cdma_plot && signals(1,1)) && ~source_input;...      % 2  - Plot CDMA PSD
        (ofdm_plot && signals(2,1)) && ~source_input;...      % 3  - Plot OFDM PSD
        (mix_plot && all(signals(1:2,1))) || source_input;... % 4  - Plot MIX (OFDM+CDMA)
        exec_sym;...                                          % 5  - Plot Symbol Rate
        exec_caf;...                                          % 6  - Plot CAF and CCAF
        plot3d.*exec_cyclo;...                                % 7  - Plot 3D SCF
        plot3d.*noise_plot.*exec_cyclo;                       % 8  - Plot 3D SCF
        plot2d.*exec_cyclo;...                                % 9 - Plot 2D cut SCF
        plot2d.*noise_plot.*exec_cyclo;...                    % 10 - Plot 2D cut noise SCF
        cdma_stem;...                   % end-4 - Stem theorectical CDMA cyc freq.
        ofdm_stem;...                   % end-3 - Stem theorectical OFDM cyc freq.
        showFigures;...                 % end-2 - Display figures
        saveEpoch;...                   % end-1 - Epoch to save
        saveFigures;...                 % end   - Saves figures to disk
        ];
    plotv(1:end-3)=plotv(1:end-3).*showFigures; % Imposition (sets all to zero if showFigures is 0)
    
    % TAG to append to saved figures 
    if ofdm_input && cdma_input && noise_input
        SIMTAG      = 'mix';              
    elseif ofdm_input && cdma_input
        SIMTAG      = 'mixNoiseFree';   
    elseif ofdm_input && ~cdma_input && noise_input
        SIMTAG      = 'ofdm';   
    elseif ofdm_input && ~cdma_input && ~noise_input
        SIMTAG      = 'ofdmNoiseFree';   
    elseif ~ofdm_input && cdma_input && noise_input
        SIMTAG      = 'cdma';   
    elseif ~ofdm_input && cdma_input && ~noise_input
        SIMTAG      = 'cdmaNoiseFree';   
    else
        SIMTAG      = 'DONOTCONSIDERME';   
    end
        
    [figs,subfig] = createhandles(plotv(1:end-5),showFigures,plotType,holdfig);
    
%     %%%%%############RECEIVER FILTER PARAMETERS#####################%%%%%%%%%%
%     %%%%%%%%%%%%%Filter parameters (if appropriate)%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     filt_type='infiniteBW'; %the type can be: 'infiniteBW', 'rect','cheb', 
%                               %'butterworth' or 'fir'. Below are some examples 
%                               %of designing the filter (if any filter is used). 
%                               %Start with no filter at all. Check the function
%                               %Fct_filterdesign_opt.m for the significance of 
%                               %the parameters below
%     rp=0.1; %loss in passband (in dB; typically <1 dB)
%     rs=40;  %attenuation in stopband, typically, >20 dB
%     tr_width=ofdm_bw/4; %transition width in Hz, as a percentage of passband  freq
%     bt_calc='passband';
%     if strcmp(filt_type,'infiniteBW')~=1,
%       filter_parameters=[tr_width rp rs];
%       [B_filter, A_filter,filter_order]= Fct_filterdesign_nonint_fs(filt_type,filter_parameters, ...
%                    fs, ofdm_bw, bt_calc);                           
%     else
%       B_filter=1; A_filter=1; 
%     end;
%     
    
    Hd=ofdmfilter(fs,fpass,fstop,Astop);
    
    
    % CONSTANTS
    ARRAY_HUGE_LEN = 0;
    % Assignments to structure
    sParameters = struct('sim',[],'source',[],'cyclo',[],'param',[],'stat',[]);
    
    % Input source
    if  source_input == 1
        sParameters.source.path       = source_path;
        fsfiles                       = zeros(source_nbFiles,1);
        lenfile                       = 0;
        for k=1:source_nbFiles
            sParameters.source.files(k,:) = [source_path,source_files(k).name];
            
            if lenfile == 0
                lenfile = length(sParameters.source.files(k,:));
            else
               assert(lenfile == length(sParameters.source.files(k,:)),'Filenames must have the same size ;)');
            end
            fsfiles(k)                  = str2num(sParameters.source.files(k,end-3:end-2)).*1e6;
        end
        sParameters.source.nbFiles    = source_nbFiles;
        sParameters.source.fs         = fsfiles;
        sParameters.source.obsTime    = source_obsTime;
        sParameters.source.chfreq     = source_chfreq;
        sParameters.source.nbObs      = source_nbObs;
        sParameters.source.vth        = source_vth;
        sParameters.source.fid        = NaN(source_nbFiles,1);
        % DO NOT REMOVE THIS!
        fs                            = fsfiles(1);
    end
    % Simulaton
    clk                           = clock;
    if source_input == 0
        sParameters.sim.nbRuns    = length(snrVal);
    else
        sParameters.sim.nbRuns    = source_nbFiles;
    end
    sParameters.sim.rfdata        = source_input;
    sParameters.sim.iterations    = numSim;
    sParameters.sim.splitSave     = splitSave;
    sParameters.sim.tag           = SIMTAG;
    sParameters.sim.cnr           = snrVal; 
    sParameters.sim.fixedPfa      = fixedPfa;
    sParameters.sim.itPfa         = itPfa;
    sParameters.sim.filter        = Hd;
%     sParameters.sim.bfilter       = B_filter;
%     sParameters.sim.afilter       = A_filter;
    sParameters.sim.BW            = ofdm_bw;
    sParameters.sim.decimation    = decimate_input;
    sParameters.sim.decFactor     = decimate_factor;   
    sParameters.sim.signals       = signals;
    sParameters.sim.inputs        = inputs;
    sParameters.sim.exec          = exec;
    sParameters.sim.plot          = plotv;
    sParameters.sim.figs          = figs;
    sParameters.sim.saveFigures   = saveFigures;
    sParameters.sim.saveEpoch     = saveEpoch;
    sParameters.sim.saveData      = saveData;
    sParameters.sim.runtime       = 0;
    sParameters.sim.folderFigures = folderFigures;
    sParameters.sim.folderData    = folderData;
    sParameters.sim.nameData      = 'scf'; 
    sParameters.sim.runTag        = runTAG;
    sParameters.sim.runDate       = [num2str(clk(3)),...
                                    '_',...
                                    num2str(clk(2)),...
                                    '_',...
                                    num2str(clk(1))];
    if recordVideo == 1
        sParameters.sim.video     = createVideo();
        if ~isempty(subfig)
            sParameters.sim.videoFig = gcf;
        else
            sParameters.sim.videoFig = figs;
        end
    else
        sParameters.sim.video     = [];
        sParameters.sim.videoFig  = [];
    end
        
    % Global parameters
    
    sParameters.param.fs          = fs;
    sParameters.param.fref        = fref;
    sParameters.param.frefOFDM    = ofdm_fref;
    sParameters.param.frefCDMA    = cdma_fref;
    sParameters.param.lenFFT      = lenFFT;
    sParameters.param.windowType  = windowing;
%     sParameters.param.alphaLags   = alphaLags;
%     sParameters.param.deltaAlpha  = deltaAlpha;
%     sParameters.param.deltaFreq   = deltaFreq;
    sParameters.param.dAlpha      = dAlpha;%/fs;
    sParameters.param.dFreq       = dFreq;%/fs;
%     sParameters.param.limAlpha    = limAlpha;
%     sParameters.param.limFreq     = limFreq;
%     
%     
    
    % Cyclostationarity algorithms
    sParameters.cyclo.psd         = zeros(ARRAY_HUGE_LEN,1);
    sParameters.cyclo.ccaf        = zeros(ARRAY_HUGE_LEN,1);
    sParameters.cyclo.caf         = zeros(ARRAY_HUGE_LEN,1);
    sParameters.cyclo.tax         = zeros(ARRAY_HUGE_LEN,1);
    sParameters.cyclo.scf         = zeros(ARRAY_HUGE_LEN,1);
    sParameters.cyclo.fax         = zeros(ARRAY_HUGE_LEN,1);
    sParameters.cyclo.scfNoise    = zeros(ARRAY_HUGE_LEN,1);
    sParameters.cyclo.alpha       = [];
    
    % Statistics
    nbCycFreq                       = length(cycFreq);
    sParameters.stat.cnr            = snrVal(1);
    sParameters.stat.saveCyc        = saveData;
    sParameters.stat.cycFreq        = cycFreq;
    if splitSave == 1
        sParameters.stat.energySignal = ones(round(numSim/10),nbCycFreq);
        sParameters.stat.energyNoise  = ones(round(numSim/10),nbCycFreq);
    else
        sParameters.stat.energySignal = [];
        sParameters.stat.energyNoise  = [];
    end
   
    
    %updates screen
    drawnow;
    
    % Sanity check
    try
        assert(splitSave == 0 || splitSave == 1);
    catch
        error('Sequence saving number should be set to 1 or 0 (disabled)');
    end
    try
        assert(any(signals(1:3)));
    catch
        error('No signals specified');
    end
    try
        assert(any(exec(1:end-1)));
    catch
        warning('No estimation will be performed');
    end
    try
        assert(sum(exec(exec(end-1):end-2)) == 1)
    catch
        error('Too many execution flags!');
    end
    
    % Validate number of carriers
    [E,F] = log2(ofdm_Ntotal);
    if E ~= 0.5 %see help log2
        error('OFDM Carriers must be multiple of 2!');
    end
        
    % Display stats
    disp('STARTING RFLAB...');
    if source_input
        disp('Using acquired RF data');
    else
        disp('Using simulated RF data');
    end
    
    if decimate_input,
        disp(['##Input signal being decimated: x',num2str(decimate_factor)]);
    end
        
    
    disp(['!! Sampling frequency assumed as: ',num2str(fs/1e6),'MHz']);
    if source_input
       disp(['fs in files: ',num2cell(fsfiles'./1e6)])
    end
end


