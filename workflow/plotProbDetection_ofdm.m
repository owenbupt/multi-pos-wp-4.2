%%%
%  ONGOING SCRIPT
%
%  This script retrieves the CF in the provided SCF (N runs) and applies
%  the normaliser method
%
%   REFERENCE
%   A Segmentation Technique Based on Standard Deviation in Body Sensor
%   Networks, http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=4454174&tag=1
%
%%% IMPORTANT
%   Energy vector is organised as follows
%       Energy = [ IDX: CF from SCF(1): ... : CF from SCF(N);]
%   The first column contains the ID in the SCF
%
%   The SCF is provided in ROW based, everything else should be organised
%   in column based
%
%   In 3D vectors the "z" dimension is the observation
%
%%% BUGS
%
%
%%% CHECK
%
%   Ratio
%   Check why there are repeated indexes!!!!!


close all;
clear variables;
addpath(genpath('../../functions'));

% PARAMETERS
plotmethod  = [ 0, 0, 0, 0, 0];   % chose what to plot, 1 - normaliser, 2 - ratio, 3 - diff
showFigures = 1;            % should figures be drawn
plotType    = 1;            % subplot or full plot
video       = 0;            % record video
showprb     = 0;
bw          = 20e6;
snrfactor   = 10*log10(bw);

% MCParameters
root        = './rfdata/';
fileset     = 'barcelona';
iter        = 100;
vCNR        = [80];
assert(root(end) == '/' || root(end) == '\');


nCNR        = length(vCNR);
interpSteps = 8000;
interpMax   = max(vCNR);
interpMin   = min(vCNR);
pcount      = 1;
figs        = createhandles(pcount,showFigures,plotType,1);
numHyp      = 4;

%ALGO PARAMETERS

szWindow     = 100;
wOffset      = 0;
previous     = 1e2;
factor       = 2.1; % choose as fs/factor


% detectionIDX = 2; % CHANGES DETECTION METHOD
% th           = 0.1;%[0:0.1:1];

totalCFs     = 2;
useonlybest  = 1; 
detectionIDX = 1; % CHANGES DETECTION METHOD
th           = 1;%[0:0.1:1];

% FOR EACH CNR FILE
cfSEL     = [];
ready     = 0;
figidx    = 0;
nbCases   = 1;
for W = 1:2
    cfset       = W;
    detectionTh = th;
    useonlybest = W-1;
    for T = 1:4
        totalCFs=T;
%         binpath = [root,'hypData.barcelona#1000$011@90D1.mat'];
%                 binpath = [root,'hypData.shch0100#1000#8.mat'];
        binpath = [root,'hypData.stch0100#1000#8.mat'];
%            binpath = [root,'hypData.stch0100#850#1.mat'];
        
        disp('ping')
        load(binpath);
        disp('pong')
        try
            sSt = sParam;
        catch
            sSt.stat = stat;
            sSt.sim  = sim;
            sSt.param = param;
            try
                sSt.cyclo.alphaFAM = sSt.stat.alphas.*sSt.param.fref;
            catch
                sSt.cyclo.alphaFAM = alphas.*sSt.param.fref;
            end
        end
        
        % sSt = stat
        scfSignal = sSt.stat.energySignal;
        scfNoise  = sSt.stat.energyNoise;
        alphas    = sSt.cyclo.alphaFAM./sSt.param.fref;
        nbObs     = size(scfSignal,1);
        
        %             assert((size(scfSignal,1) ==  size(scfNoise,1)) && (size(scfSignal,2) ==  size(scfNoise,2)));
        
        ofdm      = scfSignal;
        ofdmNoise = scfNoise;
        psymbolOFDM(1,1) = 1/sSt.sim.inputs(2,end)/sSt.param.fref;
        
        
        
        
        % DO NOT COMPUTE AFTER 1st run
        if ready == 0
            %COMPUTE ALL Cyclic Frequencies
            
            % OFDM the. cyclic frequencies
            xmin    = min(alphas);
            xmax    = max(alphas);
            psymbol = 1/sSt.sim.inputs(2,end)/sSt.param.fref;%psymbolOFDM(1,1);%
            xpos    = xmin:psymbol:xmax;
            %             xpos    = xpos.*psymbol;
            xpos(xpos<xmin)=[];
            xpos(xpos>xmax)=[];
            
            % Keep all the OFDM and merge them with CDMA frequencies
            cfOFDM = xpos(:);
            cfAll  = cfOFDM;
            
            % EXCLUDE
            cfOFDM(cfOFDM == 0) = [];
            cfAll(cfAll == 0)   = [];
%             
            cutoff                  = sSt.param.fs/(1e6*factor);
         
            cfOFDM(cfOFDM >cutoff)  = [];
            cfAll(cfAll >cutoff)    = [];
           
            cfOFDM(cfOFDM <-cutoff) = [];
            cfAll(cfAll <-cutoff)   = [];
            
            energyAll = zeros(length(cfAll),nbObs+1);
            
            cfMAP  = ones(length(cfAll),1);
            prbmap = zeros(length(cfAll),numHyp,nCNR);
            ready  = 1;
        end
        
        
        %PLOTS
        %             plot(sSt.cyclo.alphaFAM/sSt.param.fref,scfSignal(1,:), 'b-');hold on;
        %             % plot(alphas,scfSignal(1,:)./max(scfSignal(1,:))); hold on;
        %             stem(cfOFDM,0.15.*ones(length(cfOFDM),1),'x-r');
        %             stem(cfCDMA,0.15.*ones(length(cfCDMA),1),'o-y');
        
        for N=1:nbCases
            clear hitmap;
            figidx = 0;
            
            %Noise or signal
            for choice = 1 % PUT 0 back
                scfSW =[];
                scf =[];
                if choice == 1
                    hitidx = [1 2]; %SIGNAL
                    switch N
                        %                         case 1,%MIX
                        %                             scfSW = mix;
                        %                         case 2,%CMDA ONLY
                        %                             scfSW = cdma;
                        case 1,%OFDM ONLY
                            scfSW = ofdm;
                    end
                else
                    hitidx = [3 4]; %NOISE only
                    switch N
                        %                         case 1,%MIX
                        %                             scfSW = mixNoise;
                        %                         case 2,%CMDA ONLY
                        %                             scfSW = cdmaNoise;
                        case 1,%OFDM ONLY
                            scfSW = ofdmNoise;
                    end
                end
                
                
                
                
                %         %%% NOTE
                %         energy = energyAll;
                %         m=[];for k = 2:length(energy(:,1)), m(k)=(energy(k,1)-energy(k-1,1));end
                %         disp(['Mean CF distance: ',num2str(mean(m))]);
                %
                
                
                %Since now all the cf have been collected it is now time to decide if the
                %signal is present or not.
                
                % CREATE CF REGIONS
                % what to take into account
                %   Energy will contain IDX and SCF value
                %   cycFreq should be the corresponding SCF
                
                %                 cfSEL = cfOFDM;%(deselect==1);
                if useonlybest==1
                    lwend = min(fix(cfOFDM));
                    upend = max(fix(cfOFDM));
                    cfSEL = lwend:1:upend;
                    cfSEL(cfSEL == 0)=[];
%                     cfSEL = [-2 -1, 0, 1, 2];
%                     cfSEL = [-1,1];
                    disp('using "best" OFDM subset');
                else
                    cfSEL = cfOFDM;
                    disp('using all OFDM CF');
                end
                
                
                % MARKS which frequencies are now being retrieved
                cfMAP = zeros(length(cfSEL),1);
                nbCF  = length(cfSEL);
                for k=1:nbCF
                    [~,idx]    = min(abs(cfAll-cfSEL(k)));
                    cfMAP(k,1) = idx;
                    %                 energy(k,:)  = energyAll(idx,:);
                end
                
                %Retrieve energy value from SCF
                energy = zeros(nbCF,1);
                ngb = zeros(nbCF,6,nbObs);
                hitmap= zeros(nbCF,4,nbObs);
                for n=1:nbObs
                    
                    %select SCF observation
                    scf = abs(scfSW(n,:));
                    
                    % Retrieve SCF value for every K CF
                    for k = 1:nbCF
%                         if n ==1
                            [~,idx]     = min(abs(alphas-cfSEL(k)));
                            energy(k,1) = idx;                   % keep the idx
                            if k>1
                                assert(idx ~= energy(k-1,1),'Dups present'); % no repetitions
                            end
%                         else
%                             idx = energy(k,1);     % asume first observation indexes
%                         end
                        scfenergy(n,k)  = scf(idx);
                        phasecf(n,k)    = phase(scf(idx));
                        phasenoncf(n,k) = phase(scf(idx+100));
                        
                        % DO NEIGHBOUR SEARCH
                        %Check value next to each CF
                        ls  = scf(idx-wOffset-szWindow:idx-wOffset);  % Left/Down search
                        rs  = scf(idx+wOffset:idx+wOffset+szWindow);  % Right/Up search
                        fs  = scf(idx-wOffset-szWindow:idx+wOffset+szWindow); %full search
                        
                        m   = mean(fs);
                        s   = std(fs);
                        
                        ngb(k,1,n) = s/m;       % normaliser
                        ngb(k,2,n) = scf(idx)/max(scf);  % level
                        
%                         ngb(k,2,n) = mean(ls);  % left neighbours mean
                        ngb(k,3,n) = mean(rs);  % right neighbours mean
                        ngb(k,4,n) = m;         % not tested in ratio
                        ngb(k,5,n) = mean(scf(idx-wOffset-szWindow:idx))/std(scf(idx-wOffset-szWindow-1:idx-1));
                        %                 ngb(k,5,n) = mean(scf(idx-offset-szWindow:idx))/std(scf(idx-offset-szWindow:idx));
                        ngb(k,6,n) = mean([previous,scf(idx-wOffset-szWindow:idx)])/std(scf(idx-wOffset-szWindow:idx));
                        previous   = mean([previous,scf(idx-wOffset-szWindow:idx)]);
%                         plot(alphas,scf);hold on;plot(alphas(idx-wOffset-szWindow:idx+wOffset+szWindow),fs,'r','linewidth',2)
                    end % END OF FOR EACH CF
%                     scfenergy(n,:) = scfenergy(n,:) /max(scfenergy(n,:) );
                    % BUILD HITMAP for every CF
                    ph = ngb(:,detectionIDX,n)>=detectionTh;
                    nh = ngb(:,detectionIDX,n)<detectionTh;
                    
%                     plot(alphas,scf/max(scf))
%                     hold off
%                     drawnow
%                     if ~all(ph)
%                         disp(['empty@ ',num2str(n)]);
%                     end
%                         
                    
                    try
                        assert(sum(ph) + sum(nh) == size(ngb(:,detectionIDX,n),1));
                    catch
                        error('Sherlock!!')
                    end
                    hitmap(ph,hitidx(1),n)=1; % Above th
                    hitmap(nh,hitidx(2),n)=1; % Below th
                    
                end % END OF EACH OBSERVATION
                
                %                 if choice ==1
                %                     plot(figs(1),cfSEL, ngb(:,1,n));
                %                 else
                %                     plot(figs(2),cfSEL, ngb(:,1,n));
                %                 end
                %                 if any(plotmethod)
                % %                     plotNormaliserMethods
                %                 end
                %
            end % END OF RUN FOR NOISE AND SIGNAL --- HITMAP CREATED
            
            % With the HITMAP constructed retrieve the Probability for EACH SET
            % FOR ALL OBSERVATIONS
            %   Compute probability
            prbcf = zeros(nbCF,4);
            for k=1:nbCF % cleared every N
                hit   = hitmap(k,1,:); %HIT
                miss  = hitmap(k,2,:); %MISS
                fhit  = hitmap(k,3,:); %False Positive
                fmiss = hitmap(k,4,:); %Good False
                prbmap(cfMAP(k),1,T) = sum(hit(1,:))/length(hit(1,:));
                prbmap(cfMAP(k),2,T) = sum(miss(1,:))/length(miss(1,:));
                prbmap(cfMAP(k),3,T) = sum(fhit(1,:))/length(fhit(1,:));
                prbmap(cfMAP(k),4,T) = sum(fmiss(1,:))/length(fmiss(1,:));
%                 disp(['#CF: ',num2str(cfSEL(k),'%06.2f'),...
%                     ': T.POS ',num2str(sum(hit(1,:)),'%04d'),...
%                     ': F.NEG ',num2str(sum(miss(1,:)),'%04d'),...
%                     ': F.POS ',num2str(sum(fhit(1,:)),'%04d'),... %Noise HIT is Value < TH
%                     ': T.NEG ',num2str(sum(fmiss(1,:)),'%04d'),...
%                     ]);
                
                prbcf(k,1) = prbmap(cfMAP(k),1,T);
                prbcf(k,2) = prbmap(cfMAP(k),2,T);
                prbcf(k,3) = prbmap(cfMAP(k),3,T);
                prbcf(k,4) = prbmap(cfMAP(k),4,T);
                
                %save to xls
                assert(sum(hit(1,:))   + sum(miss(1,:)) == nbObs);
                %                 assert(sum(fmiss(1,:)) + sum(fhit(1,:)) == nbObs);
            end
            
            % COMPUTE MEAN PROBABILITY over CF SET
            pd(N,T)   = sum(sum(hitmap(:,1,:)) > totalCFs)/nbObs;%mean(prbcf(:,1));
            pnd(N,T)  = 1-pd(N,T);%mean(prbcf(:,2));
            fpd(N,T)  = sum(sum(hitmap(:,3,:)) > totalCFs)/nbObs;
            fpnd(N,T) = mean(prbcf(:,4));
            
            %%% Or, should prob > th, then hit?
            
            % Probability of detection and cyclic frequency
%             if showprb == 1
%                 figidx=figidx+1;
%                 stem(figs(figidx),cfSEL,prbmap(cfMAP(:),1,T),'x');
%                 title(figs(figidx),['Probability of detection @Cyc. Freq ']);
%             end
            
            % Tell me which CF have the biggest probability
%             disp('-------------');
% %             disp(['STAT FOR CNR: ',num2str(vCNR(T))]);
%             disp('TOP 5 - CF Probability ( CF | Pd )')
%             
            clear sorted
            try
                sorted(:,1)      = cfSEL;
                sorted(:,2)      = prbmap(cfMAP,1,T);
                sorted           = sortrows(sorted(:,:),-2); % Sort by first row descending
                
                aboveZero        = sum(sorted(:,2) > 0);
                percFreq         = sum(sorted(:,2) > 0)/length(sorted(:,2));
                
%                 disp([sorted(1:5,1),sorted(1:5,2)]); % Disp top 10
%                 disp(['#CF>0  cyclic frequencies CONSIDERED: ',num2str(aboveZero)]);
%                 disp(['#TOTAL cyclic frequencies CONSIDERED: ',num2str(length(sorted(:,1)))]);
%                 disp(['%TOTAL cyclic frequencies CONSIDERED: ',num2str(percFreq*100)]);
            end
            thPD(N,T,W)  = pd(N,T);
            thFPD(N,T,W) = fpd(N,T);
            disp(['> ',num2str(totalCFs),' %'])
            disp(['PD: ',num2str(pd*100),' %'])
            disp(['PFA: ',num2str(fpd*100),' %'])
%             drawnow
        end % END of each CF SET
        
        
    end %end of each CNR file
    
end
%%
% PLOT PD over CNR for each set of CF ( ALL, CDMA, OFDM)

results = [{thPD},{thFPD}];
disp(['> ',num2str(totalCFs),' %'])
disp(['PD: ',num2str(pd*100),' %'])
disp(['PFA: ',num2str(fpd*100),' %'])

% publishplots;

% disp(['> ',num2str(totalCFs),' %'])
% disp(['PD: ',num2str(pd*100),' %'])
% disp(['PFA: ',num2str(fpd*100),' %'])



% Some plot tools

% See what reagion you are extracting data from
% figure;plot(scf);hold on;plot(idx-wOffset-szWindow:idx+wOffset+szWindow,fs,'r','linewidth',2)




