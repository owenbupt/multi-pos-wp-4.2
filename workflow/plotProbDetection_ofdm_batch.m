%%%
%  ONGOING SCRIPT
%
%  This script retrieves the CF in the provided SCF (N runs) and applies
%  the normaliser method
%
%   REFERENCE
%   A Segmentation Technique Based on Standard Deviation in Body Sensor
%   Networks, http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=4454174&tag=1
%
%%% IMPORTANT
%   Energy vector is organised as follows
%       Energy = [ IDX: CF from SCF(1): ... : CF from SCF(N);]
%   The first column contains the ID in the SCF
%
%   The SCF is provided in ROW based, everything else should be organised
%   in column based
%
%   In 3D vectors the "z" dimension is the observation
%
%%% BUGS
%
%
%%% CHECK
%
%   Ratioaddpath(genpath('./../functions'));
addpath(genpath('./workflow'));
%   Check why there are repeated indexes!!!!!

close all;
clear variables;
addpath(genpath('./../functions'));

% PARAMETERS
bw           = 20e6;
snrfactor    = 10*log10(bw);

% MCParameters
root         = './rfdata/';
assert(root(end) == '/' || root(end) == '\');

ready        = 0;
batch        = 1;
test_noise   = 0;     % Skip noise

useonlybest  = 0;
vth          = 1;
szWindow     = 100;
wOffset      = 0;
factor       = 2.1; % choose as fs/factor


% Retrieve file list
load_data = 1;
if batch == 1
    path     = 'D:\gnuradio\ch1\scf\10\';
    files    = dir([path,'*.mat']);
    nbFiles  = size(files,1);
    fidx     = 1;
else
    filename = 'scf.tut01f10#1#1.mat';
end

prealocate = 1;
N          = 0;
% For all the given files
for nFile = 1:nbFiles
    if load_data == 1
        % Load data
        if batch == 1
            load_data  = 1;
            binpath    = [path,files(fidx,:).name];
            fidx       = fidx + 1;
            itmult     = nbFiles;
        else
            load_data  = 0;
            binpath    = [root,filename];
            itmult     = 1;
        end
        disp(['Loading file ',binpath]);
        load(binpath);

        sSt.stat = stat;
        sSt.sim  = sim;
        sSt.param = param;
        scfSignal = sSt.stat.energySignal;
        scfNoise  = sSt.stat.energyNoise;
        
%         load('alphasfs40d4');
%         sSt.stat.alphas = alphadomain;
%         alphas = alphadomain;
%         nbObs = 650;
%         
        try
            sSt.cyclo.alphaFAM = sSt.stat.alphas.*sSt.param.fref;
        catch
            sSt.cyclo.alphaFAM = alphas.*sSt.param.fref;
        end

        % Organise data
        nbObs     = size(scfSignal,1);
        alphas    = sSt.cyclo.alphaFAM./sSt.param.fref;
        
        if test_noise == 1;
            scfObs  = sSt.stat.energyNoise;
        else
            scfObs = sSt.stat.energySignal;
        end

        % OFDM the. cyclic frequencies
        xmin             = min(alphas);
        xmax             = max(alphas);
        psymbol          = 1/4;%1/sSt.sim.inputs(2,end)/sSt.param.fref;
        xpos             = xmin:psymbol:xmax;
        xpos(xpos<xmin)  = [];
        xpos(xpos>xmax)  = [];

        % Keep all the OFDM
        cfOFDM = xpos(:);
        cfAll  = cfOFDM;

        % EXCLUDE
        cutoff                  = sSt.param.fs/(1e6*factor);
        cfOFDM(cfOFDM >cutoff)  = [];
        cfAll(cfAll >cutoff)    = [];
        cfOFDM(cfOFDM <-cutoff) = [];
        cfAll(cfAll <-cutoff)   = [];
%         cfOFDM(cfOFDM == 0)     = [];
%         cfAll(cfAll == 0)       = [];

        % CREATE CF REGIONS
        if useonlybest==1
            disp('Using best OFDM subset');
            lwend             = min(fix(cfOFDM));
            upend             = max(fix(cfOFDM));
            cfSEL             = lwend:1:upend;
%             cfSEL(cfSEL == 0) = [];
        else
            disp('Using entire set OFDM CF');
            cfSEL = cfAll;
        end

        % Preallocate
        if prealocate == 1
            prealocate = 0;
            nbCF       = length(cfSEL);
            prbmap     = zeros(length(cfSEL),2,1);
            scfenergy  = zeros(nbObs*itmult,nbCF);
            hitmap     = zeros(nbCF,2,nbObs*itmult);
        end
    end

    % Retrieve SCF values
    disp(['Observation ',num2str(N)]);
    for n=1:nbObs
        % Global counter
        N=N+1;
        
        %select SCF observation
        scf = abs(scfObs(n,:));

        % Retrieve SCF value for every K CF
        for k = 1:nbCF
            [~,idx]         = min(abs(alphas-cfSEL(k)));
            scfenergy(N,k)  = scf(idx);

            % DO NEIGHBOUR SEARCH
            ls  = scf(idx-wOffset-szWindow:idx-wOffset);  % Left/Down search
            rs  = scf(idx+wOffset:idx+wOffset+szWindow);  % Right/Up search
            fs  = scf(idx-wOffset-szWindow:idx+wOffset+szWindow); %full search

            m   = mean(fs);
            s   = std(fs);

            ngb(k,1,n) = s/m;       % normaliser
            value_i(N,k) = s/m;
            value_s(N,k) = s;
            value_m(N,k) = m;

        end % END OF FOR EACH CF

        % BUILD HITMAP for every CF
        ph             = ngb(:,1,n) >= vth;
        nh             = ngb(:,1,n) <  vth;
        hitmap(ph,1,N) = 1; % Above th
        hitmap(nh,2,N) = 1; % Below th
        try
            assert(sum(ph) + sum(nh) == size(ngb(:,1,n),1));
        catch
            error('Sherlock!!')
        end
        
    end % END of Files
    disp(['Finished file ',num2str(nFile),', Observation: ',num2str(N)]);
    
end %END of all Files

% Compute probability
for k=1:nbCF
    hit   = hitmap(k,1,:); % hits for all observations
    miss  = hitmap(k,2,:); % misses for all observations

    prbmap(k,1) = sum(hit(1,:))/length(hit(1,:));
    prbmap(k,2) = sum(miss(1,:))/length(miss(1,:));
end

%save to xls
assert(sum(prbmap(:,1)+prbmap(:,2))/size(prbmap,1) == 1);

% COMPUTE MEAN PROBABILITY over CF SET
for totalCFs = 1:4
pd(totalCFs)  = sum(sum(hitmap(:,1,:)) > totalCFs)/N;
end
disp(pd)
results = [{cfSEL},{prbmap(:,1)},{scfenergy},{value_i},{value_m},{value_s}];
save(['./resultsBarcelona/','ALL_fix_results_vth_',num2str(vth),'_fs_',num2str(path(end-2:end-1),'%02d'),'.mat'],'results')
% publishplots;
% disp(['> ',num2str(totalCFs),' %'])
% disp(['PD: ',num2str(pd*100),' %'])

% Some plot tools
% See what reagion you are extracting data from
% figure;plot(scf);hold on;plot(idx-wOffset-szWindow:idx+wOffset+szWindow,fs,'r','linewidth',2)
%
%%EOF
