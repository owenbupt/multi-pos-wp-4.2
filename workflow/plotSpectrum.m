function energy = plotSpectrum(sSt,simTime,cdma,ofdm,txsignal,txnoise)
    %PLOTSPECTRUM uses the existing axes to plot the spectrum of the signals
    
    % Control variables
    ctl      = sSt.sim.plot;
    figs     = sSt.sim.figs;
    
    % Parameters
    fs       = sSt.param.fs;
    fax      = sSt.cyclo.fax;
    alpha    = sSt.cyclo.alpha/sSt.param.fref;
    scf      = sSt.cyclo.scf;
    scfNoise = sSt.cyclo.scfNoise;
    
    %%% Plots
    % WARNING
    % Pay attention to the Indexes!
    
    if any(ctl)
        figidx   = 0;
        stem_mag = 0.3;
        
        % 1 - NOISE
        figidx = figidx+1;
        if ctl(figidx)
            plotPSD(figs(figidx),fs,txnoise,'WGN')
        end
        
        % 2 - CDMA
        figidx = figidx+1;
        if ctl(figidx)
            plotPSD(figs(figidx),fs,cdma,'CDMA')
        end
        
        % 3 - OFDM
        figidx = figidx+1;
        if ctl(figidx)
            plotPSD(figs(figidx),fs,ofdm,'OFDM');
%               plotPSD(figs(figidx),fs,txsignal,'OFDM')
        end
        

        % 4 - SIGNAL MIXTURE
        figidx = figidx+1;
        if ctl(figidx)
            plotPSD(figs(figidx),fs,txsignal,'TX Signal');
        end
        
        
        % 5 - Symbol Rate
        figidx = figidx+1;
        if ctl(figidx)
            plot(figs(figidx),alpha-0.5,fftshift(J));
            xlabel('Cyclic frequency \alpha');
            disp(['Symbol rate ',num2str(a)]);
        end
        
        % 6 - CAF and CCAF
        figidx = figidx+1;
        if ctl(figidx)
            plot3DPSD(figs(figidx),sSt.cyclo.tax,...                 % x
                sSt.cyclo.alpha./sSt.param.fref,...                  % y
                abs(sSt.cyclo.caf)./max(max(abs(sSt.cyclo.caf))),... % z
                '\tau','\alpha','CAF')
        end
        
        
        % 3D PLOT
        figidx = figidx+1;
        if ctl(figidx)
            plot3DPSD(figs(figidx),...
                alpha,...       % x
                fax,...                        % y
                abs(scf)./max(max(abs(scf))),... % z
                '\alpha','fax','SCF');
        end
        
        % 3D PLOT Noise
        figidx = figidx+1;
        if ctl(figidx)
            plot3DPSD(figs(figidx),fax,...       % x
                alpha,...                        % y
                abs(scfNoise)./max(max(abs(scfNoise))),... % z
                'f','\alpha','FFT - Tx SCF');
        end
        
        
        % SCF 2D cut (SCF(f,alpha))
        figidx = figidx+1;
        if ctl(figidx)
            [~, f0]                         = min(abs(fax));
            plot(figs(figidx),alpha, abs(scf(f0,:))./max(abs(scf(f0,:))), 'b-');
%             for k=1:length(fax), plot(alpha,abs(scf(k,:))./max(abs(scf(k,:)))); drawnow;disp(k); pause; end;
            xlabel(figs(figidx),'Cyclic frequency [Mhz]');
            ylabel(figs(figidx),'Normalised value');
            %             title(figs(figidx),['FAM: SCF(\alpha,0) for f_s=', num2str(fs/1e6), ' MHz, \Delta f/\Delta \alpha=', num2str(sSt.param.deltaFreq/sSt.param.deltaAlpha)]);
            ylim(figs(figidx),[0 1]);
            hold(figs(figidx),'on');
            % CDMA the. cyclic frequencies
            if ctl(end-4)
                xmin    = min(alpha)*50;
                xmax    = max(alpha)*50;
                psymbol = sSt.sim.inputs(1,end)/sSt.param.fref;
                xpos    = xmin:1:xmax;
                xpos    = xpos.*psymbol;
                xpos(xpos<xmin)=[];
                xpos(xpos>xmax)=[];
%                 ypos    = stem_mag*ones(1,length(xpos));
                
                aux     = xpos;
                psymbol = sSt.sim.inputs(1,end-1);
                xpos    = xmin:1:xmax;
                xpos    = xpos.*psymbol;
                xpos(xpos<xmin) = [];
                xpos(xpos>xmax) = [];
%                 ypos    = stem_mag*ones(1,length(xpos));
                ypos    = stem_mag*ones(1,length(unique([xpos(:);aux(:)])'));
                cfCDMA  = unique([xpos(:);aux(:)]);
                stem(figs(figidx),cfCDMA', ypos, 'g-.', 'o','linewidth',0.5,'MarkerFaceColor','g');
            end
            
            % OFDM the. cyclic frequencies
            if ctl(end-3)
                xmin    = min(alpha);
                xmax    = max(alpha);
                psymbol = 1/sSt.sim.inputs(2,end)/sSt.param.fref;
                xpos    = xmin*70:1:xmax*70;
                xpos    = xpos.*psymbol;
                xpos(xpos<xmin)=[];
                xpos(xpos>xmax)=[];
                ypos    = stem_mag*ones(1,length(xpos));
                cfOFDM  = xpos;
                stem(figs(figidx), cfOFDM, ypos, 'r-.', 'x','MarkerFaceColor','r','MarkerSize',8);
            end
            if ctl(end-3)&& ctl(end-4)
                legend(figs(figidx),'SCF(\alpha,0)','CF_{CDMA}','CF_{OFDM}');
                %             xlim(figs(figidx),[-70/4,70/4])
            end
            xlim([min(alpha) max(alpha)])
            hold(figs(figidx),'off');
        end
        
        % SCF 2D cut (SCF(f,alpha)) NOISE
        figidx = figidx+1;
        if ctl(figidx)
            [~, f0]                         = min(abs(fax));
            plot(figs(figidx),alpha, abs(scfNoise(f0,:))./max(abs(scfNoise(f0,:))), 'b-');
            
            xlabel(figs(figidx),'Cyclic frequency [Mhz]');
            ylabel(figs(figidx),'Normalised value');
            %             title(figs(figidx),['FAM: SCF(\alpha,0) for f_s=', num2str(fs/1e6), ' MHz, \Delta f/\Delta \alpha=', num2str(sSt.param.deltaFreq/sSt.param.deltaAlpha)]);
            ylim(figs(figidx),[0 1]);
            hold(figs(figidx),'on');
            % CDMA the. cyclic frequencies
            if ctl(end-4)
                xmin    = min(alpha);
                xmax    = max(alpha);
                psymbol = sSt.sim.inputs(1,end)/sSt.param.fref;
                xpos    = xmin:1:xmax;
                xpos    = xpos.*psymbol;
                xpos(xpos<xmin)=[];
                xpos(xpos>xmax)=[];
                ypos    = stem_mag*ones(1,length(xpos));
                
                aux     = xpos;
                psymbol = sSt.sim.inputs(1,end-1);
                xpos    = xmin:1:xmax;
                xpos    = xpos.*psymbol;
                xpos(xpos<xmin) = [];
                xpos(xpos>xmax) = [];
                ypos    = stem_mag*ones(1,length(xpos));
                ypos    = stem_mag*ones(1,length(unique([xpos(:);aux(:)])'));
                stem(figs(figidx),unique([xpos(:);aux(:)])', ypos, 'g-.', 'o','linewidth',0.5,'MarkerFaceColor','g');
                cfCDMA = unique([xpos(:);aux(:)]);
            end
            
            % OFDM the. cyclic frequencies
            if ctl(end-3)
                xmin    = min(alpha);
                xmax    = max(alpha);
                psymbol = 1/sSt.sim.inputs(2,end)/sSt.param.fref;
                xpos    = xmin*10:1:xmax*10;
                xpos    = xpos.*psymbol;
                xpos(xpos<xmin)=[];
                xpos(xpos>xmax)=[];
                ypos    = stem_mag*ones(1,length(xpos));
                stem(figs(figidx),xpos, ypos, 'r-.', 'x','MarkerFaceColor','r','MarkerSize',8);
                cfOFDM = xpos;
            end
            
            legend(figs(figidx),'SCF(\alpha,0)','CF_{CDMA}','CF_{OFDM}');
            xlim(figs(figidx),[-70/4,70/4])
            hold(figs(figidx),'off');
        end
        
        
        drawnow;
        
        % Save pictures
        if ctl(end)
            if simTime == ctl(end-1)
                filename = [sSt.sim.folderFigures,'/',sSt.sim.tag,'.CNR',num2str(sSt.stat.cnr),'/'];
                if ~exist(filename,'dir')
                    mkdir(filename);
                end
                outputfig([filename,'@OBS_',num2str(simTime)],figs);
            end
        end
    end
    %     set(gcf, 'Position', [280 70 800 600]); % Maximize figure.
end



          
% the error looks for the max in all alphas, from f0 to fs/2.
%plot(figs(figidx), sSt.param.alphaLags./sSt.param.fref,abs(fftSCF(:,f0FFT:end))/max(abs(fftSCF(:,f0FFT:end))),'b.-'); %My random mistake actually produces a good thing :P
%             plot(figs(figidx), sSt.param.alphaLags./sSt.param.fref,abs(sSt.cyclo.scfFFTNoise(:,f0FFT))./max(abs(sSt.cyclo.scfFFTNoise(:,f0FFT))), 'b.-');
%             title(figs(figidx),['FFT_SCF(0,\alpha) for f_s = ', num2str(fs/1e6),'MHz and f_{ref} = ',num2str(sSt.param.fref/1e6),'MHz']);
%             xlabel(figs(figidx),'\alpha*f_s/f_{ref}');