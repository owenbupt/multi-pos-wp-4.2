thfigs    = createhandles(ones(nbCases*2,1),1,1);
markers = {'+','o','*','.','x','s','d','^','v','>','<','p','h'};
fnb = 1:nbCases*2;
idx = 0;
for M=1:nbCases
   idx = idx+1;
    aux(:,1:size(thPD,3))=thPD(M,:,:);
    for k=1:size(aux,2)
        plot(thfigs(fnb(idx)),vCNR,aux(:,k),['k',markers{k},'-']);hold on;
        
    end
    
    th = 0.1:0.1:1;
    legendCell = cellstr(num2str(th', 'V_{th}=%01.2f '));
    legend(thfigs(fnb(idx)),legendCell);
    legend(thfigs(fnb(idx)),'Location','EastOutside')
    title(thfigs(fnb(idx)),['Probability of Detection ','(CASE ',num2str(M),')'])
    xlabel(thfigs(fnb(idx)),'CNR')
    ylabel(thfigs(fnb(idx)),'Probability')
    grid(thfigs(fnb(idx)),'on')
    
    idx = idx+1;
    aux(:,1:size(thPD,3))=thFPD(M,:,:);
    for k=1:size(aux,2)
        plot(thfigs(fnb(idx)),vCNR,aux(:,k),['r',markers{k},'-']);hold on;        
    end
    
    th = 0.1:0.1:1;
    legendCell = cellstr(num2str(th', 'V_{th}=%01.2f '));
    legend(thfigs(fnb(idx)),legendCell);
    legend(thfigs(fnb(idx)),'Location','EastOutside')
    title(thfigs(fnb(idx)),['Probability of False Detection ','(CASE ',num2str(M),')'])
    xlabel(thfigs(fnb(idx)),'CNR')
    ylabel(thfigs(fnb(idx)),'Probability')
    grid(thfigs(fnb(idx)),'on')
end


% ROC curve
froc = createhandles(1,1,1);
marker = {'+','o','*','.','x','s','d','^','v','>','<','p','h','+','o','*','.','x','s','d','^','v','>','<','p','h'};
legendCell = cellstr(num2str(vCNR', 'CNR=%01.2f '));
cm =colormap('jet');
for cnr=1:size(thPD,2)
    pdcnr(1,:)  = thPD(1,cnr,:);
    fpdcnr(1,:) = thFPD(1,cnr,:);
    plot(froc,fpdcnr,pdcnr,'color',cm(cnr*2,:),'Marker',marker{cnr});
end
legend(froc,legendCell);
legend(froc,'Location','EastOutside')
xlabel(froc,'Probability of False Detection')
ylabel(froc,'Probability of Detection')
title(froc,'V_{th} = 0.1:1')
