%% 
%Options
printout = [1 1 1 1 1 1 0];
basename = 'set';
%Global
sets  = [{'A_S'},{'B_S'}];
%Plot Pd figures for each set

bw = 20e6;snrfactor = 10*log10(bw);
pd  = results{1};
fpd = results{2};
pfa = mean(fpd(1,:));

% vCNR    = [50 55 60 65 70 75 80];
x  = (vCNR)-snrfactor;

%% pdcdma
try
ctag = [{'bo'},{'rs'},{'g^'}];
pdA(:,:) = pd(:,:,1);
dName = [{['$P( {{\cal H}}_{1.1} | ',sets{1},' )$']},{['$P( {{\cal H}}_{1.2} | ',sets{1},' )$']},{['$P({{\cal H}}_{1.3} | ',sets{1},' )$']}];
close all; h = createhandles(1,1,1); prettyplot(h,x,pdA,['Probability of detection for $\cal{A}_{\cal{S}}$ with $P_{fa} = ',num2str(pfa,'%2.3f\n'),'$'],ctag,dName)
if printout(1)
   outputfig([basename,sets{1}(1)],h);
end
 
end
%% pdofdm
ctag = [{'bo'},{'rs'},{'g^'}];
pdB(:,:) = pd(:,:,2);
dName = [{['$P({\cal H}_{1.1} | ',sets{2},' )$']},{['$P( {{\cal H}}_{1.2} | ',sets{2},' )$']},{['$P({{\cal H}}_{1.3} | ',sets{2},' )$']}]; 
close all; h = createhandles(1,1,1); prettyplot(h,x,pdB,['Probability of detection for $\cal{B}_{\cal{S}}$ with $P_{fa} = ',num2str(pfa,'%2.3f\n'),'$'],ctag,dName)
if printout(2)
   outputfig([basename,sets{2}(1)],h);
end


%% merged
ctag = [{'rs'},{'g^'}];
icurve = 1;
pdAB(1,:) = pdA(icurve,:);
pdAB(2,:) = pdB(icurve,:);

dName = [{['$P( {{\cal H}}_{1.1} | ',sets{1},' )$']},{['$P( {{\cal H}}_{1.1} | ',sets{2},' )$']}];
close all; h = createhandles(1,1,1); prettyplot(h,x,pdAB,['Probability of detection $P_{fa} = ',num2str(pfa,'%2.3f\n'),'$'],ctag,dName)
if printout(3)
   outputfig([basename,sets{1}(1),sets{2}(1)],h);
end


%% CDMA power boost 
ctag = [{'rs'},{'g^'}];
icurve = 1;
pdAboost(1,:) = pdA(icurve,:);
pdAboost(2,:) = pdB(icurve,:);

dName = [{['$P( {\cal H}_{1.1} | ',sets{1},' )$']},{['$P( {\cal H}_{1.1} | ',sets{2},' )$']}];
close all; h = createhandles(1,1,1); prettyplot(h,x,pdAboost,['Boosted CDMA (+3dB) $P_{fa} = ',num2str(pfa,'%2.3f\n'),'$'],ctag,dName)
if printout(4)
   outputfig([basename,sets{1}(1),'boost'],h);
end


%% OFDM power boost
ctag = [{'rs'},{'g^'}];
icurve = 1;
pdBboost(1,:) = pdA(icurve,:);
pdBboost(2,:) = pdB(icurve,:);

dName = [{['$P( {\cal H}_{1.1} | ',sets{1},' )$']},{['$P( {\cal H}_{1.1} | ',sets{2},' )$']}];
close all; h = createhandles(1,1,1); prettyplot(h,x,pdBboost,['Boosted OFDM (+3dB) $P_{fa} = ',num2str(pfa,'%2.3f\n'),'$'],ctag,dName)
if printout(5)
   outputfig([basename,sets{2}(1),'boost'],h);
end
