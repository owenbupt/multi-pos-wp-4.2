function recordKeeping(sSt,idx)
%RECORDKEEPING saves and prints information about data
%
%   AUTHOR
%   Pedro Silva
%   Tampere University of Technology
    
    % Print some information
        
    % Save to disk
    if sSt.sim.saveData && ~sSt.sim.splitSave
        % Basename (no need for .mat)
        if ~sSt.sim.rfdata
            filename =[sSt.sim.folderData,...
                sSt.sim.nameData,'.',...
                sSt.sim.runTag,'#',...
                num2str(sSt.sim.iterations),'$',...
                num2str(sSt.sim.signals(1)),... %cdma
                num2str(sSt.sim.signals(2)),... %ofdm
                num2str(sSt.sim.signals(3)),'@',...
                num2str(sSt.stat.cnr),'D',num2str(sSt.sim.decFactor)];
        else
            filename =[sSt.sim.folderData,...
                sSt.sim.nameData,'.',...
                sSt.source.files(idx,end-9:end-2),'#',...
                num2str(sSt.sim.iterations),'#',...
                num2str(sSt.sim.decFactor)];
        end
        savetodisk(filename,sSt,0);
    end

%TODO: needs redesign    
%     disp('---------')
%     disp(['Memory loss ',num2str(profileMemory(sSt.stat.memory,whos('sSt'))),' bytes']);
%     disp(['Execution time ',num2str(sSt.sim.runtime),' seconds']);
%     disp('---------')
%     disp('All done');
    
    
end


function diffBytes = profileMemory(prevState,newState)
    %PROFILEMEMORY Computes memory metrics
    
    %For each structure get total bytes
    prevBytes = 0;
    for k=1:size(prevState,1)
        prevBytes = prevBytes + (prevState(k).bytes);
    end
    
    newBytes = 0;
    for k=1:size(newState,1)
        newBytes = newBytes + (newState(k).bytes);
    end
    
    diffBytes = (newBytes - prevBytes);
    
end




