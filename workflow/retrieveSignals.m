function [txSignal,txNoise,pfid]=retrieveSignals(fid,filename,fs,obsTime,nbObs,vth)
%retrieveSignals obtains signal data from binary files
%   The function accepts N files as an input
%
%   DESCRIPTION
%   The function reads from the given files
%           fs*obsTime 
%   complex values.
%   The observation is saved into a 3D vector as (obs,time,file).
%
%   INPUT
%   path     - path to files
%   filename - file(s) name(s)
%   fs       - data sampling frequency
%   obsTime  - observation time
%   nbObs    - number of observations to be done
%   vth      - noise threshold
%
%   OUTPUT
%   txSignal - Signal obtained from files (window x N_Files)
%   txNoise  - Approximation to signal noise (all signal below Vth)

    readSize = round(fs*obsTime);       
    txSignal(readSize,nbObs,1) = 0;
    for t=1:nbObs        
            [txSignal(:,t), pfid] = sequential_read_complex_binary(fid, filename, readSize);
            
            % Retrieve noise
            txNoise        = txSignal(:,t);
            nPos           = abs(txSignal(:,t))>vth;
            txNoise(nPos)  = [];
        
        if(any(pfid) == 0)
            break;
        end
    end
end